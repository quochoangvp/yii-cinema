// find menu-item associated with this page and make current:
$(document).ready(function() {
    $('#w1-collapse a').each(function(index, value) {
        if ($(this).prop('href') === window.location.href) {
            $(this).parent().addClass('active');
        }
    });
    $('#w0-collapse a').each(function(index, value) {
        if ($(this).prop('href') === window.location.href) {
            $(this).parent().addClass('active');
        }
    });
});
