<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CinemaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý thông tin rạp chiếu phim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cinema-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm rạp chiếu phim mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'image',
                'value' => function($model)
                {
                    return Url::to('@images/uploads/' . $model->image);
                },
                'format' => ['image',['width'=>'auto','height'=>'50']],
            ],
            'name',
            'address',
            'slot',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
