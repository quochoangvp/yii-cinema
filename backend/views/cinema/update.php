<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cinema */

$this->title = 'Cập nhật thông tin rạp chiếu phim: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách rạp chiếu phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="cinema-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
