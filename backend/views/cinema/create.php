<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cinema */

$this->title = 'Thêm rạp chiếu phim mới';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách rạp chiếu phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cinema-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
