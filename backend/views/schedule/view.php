<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Film;
use common\models\Cinema;

/* @var $this yii\web\View */
/* @var $model common\models\Schedule */

$this->title = 'Lịch chiếu phim ' . Film::find()->where(['id' => $model->film_id])->one()->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách lịch chiếu', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn muốn xóa?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'film_id',
                'value' => Film::find()->where(['id' => $model->film_id])->one()->name
            ],
            [
                'attribute' => 'cinema_id',
                'value' => Cinema::find()->where(['id' => $model->cinema_id])->one()->name
            ],
            'time',
            [
                'attribute' => 'status',
                'value' => ($model->status) ? 'Đã xác nhận': 'Đã hủy'
            ],
        ],
    ]) ?>

</div>
