<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Film;
use common\models\Cinema;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ScheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lịch chiếu phim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm lịch chiếu mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'film_id',
                'value' => function($model)
                {
                    return Film::find()->where(['id' => $model->film_id])->one()->name;
                }
            ],
            [
                'attribute' => 'cinema_id',
                'value' => function($model)
                {
                    return Cinema::find()->where(['id' => $model->cinema_id])->one()->name;
                }
            ],
            'time',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    if ($model->status === 1) {
                        return 'Đang hoạt động';
                    } else {
                        return 'Ngưng hoạt động';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
