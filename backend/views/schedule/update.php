<?php

use yii\helpers\Html;
use common\models\Film;

/* @var $this yii\web\View */
/* @var $model common\models\Schedule */

$this->title = 'Cập nhật lịch chiếu phim: ' . Film::find()->where(['id' => $model->film_id])->one()->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách lịch chiếu phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="schedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
