<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Film;
use common\models\Cinema;
use yii\jui\DatePicker;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Schedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schedule-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field($model, 'film_id')->dropdownList(
            Film::find()->select(['name', 'id'])->indexBy('id')->column(),
            ['prompt'=>'Chọn phim']
        );
    ?>

    <?php
        echo $form->field($model, 'cinema_id')->dropdownList(
            Cinema::find()->select(['name', 'id'])->indexBy('id')->column(),
            ['prompt'=>'Chọn rạp']
        );
    ?>

    <?= $form->field($model, 'time')->widget(DateTimePicker::classname(), [
        'name' => 'time',
        'type' => DateTimePicker::TYPE_INPUT,
        'value' => date_format(date_create(), 'Y-m-d h:i:s'),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd hh:ii:ss'
        ],
        'language' => 'vi'
    ]); ?>

    <?= $form->field($model, 'status')->dropDownList(
        [
            '1' => 'Hoạt động',
            '0' => 'Không hoạt động'
        ]
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
