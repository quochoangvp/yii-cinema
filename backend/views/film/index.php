<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\FilmType;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FilmSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Quản lý phim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm phim mới', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Loại phim', Url::to('@web/film-type'), ['class' => 'btn btn-default']) ?>
        <?= Html::a('Danh mục phim', Url::to('@web/film-cate'), ['class' => 'btn btn-default']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            // 'slug',
            [
                'attribute' => 'type',
                'value' => function($model)
                {
                    return FilmType::find()->where(['id' => $model->type])->one()->name;
                }
            ],
            'start',
            'end',
            // 'cate_id',
            // 'actors',
            'director',
            'producer',
            // 'thumb',
            // 'sumary:ntext',
            // 'details:ntext',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    if ($model->status === 1) {
                        return 'Hiển thị';
                    } else {
                        return 'Ẩn';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
