<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\FilmCate;
use common\models\FilmType;

/* @var $this yii\web\View */
/* @var $model common\models\Film */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn chắc chắn muốn xóa phim này?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php if ($model->thumb): ?>
    <p>
        <?= Html::img(Url::to('@images/uploads/' . $model->thumb)); ?>
    </p>
    <?php endif ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'trailer_link',
                'value' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/PWcimiuIGxM" frameborder="0" allowfullscreen></iframe>',
                'format' => 'raw',
            ],
            'name',
            'slug',
            [
                'attribute' => 'type',
                'value' => FilmType::find()->where(['id' => $model->type])->one()->name
            ],
            'start',
            'end',
            [
                'attribute' => 'cate_id',
                'value' => FilmCate::find()->where(['id' => $model->cate_id])->one()->name
            ],
            'actors',
            'director',
            'producer',
            'sumary:ntext',
            'details:ntext',
            [
                'attribute' => 'status',
                'value' => ($model->status) ? 'Hiển thị': 'Ẩn'
            ],
        ],
    ]) ?>

</div>
