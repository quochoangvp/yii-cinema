<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FilmCate;
use common\models\FilmType;
use common\models\Actor;
use common\models\Director;
use common\models\Producer;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Film */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="film-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'trailer_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'start')->widget(DatePicker::classname(), [
        'language' => 'vi',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control'],
    ]) ?>

    <?= $form->field($model, 'end')->widget(DatePicker::classname(), [
        'language' => 'vi',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control'],
    ]) ?>

    <?php
        echo $form->field($model, 'cate_id')->dropdownList(
            FilmCate::find()->select(['name', 'id'])->indexBy('id')->column(),
            ['prompt'=>'Chọn thể loại']
        );
    ?>

    <?= $form->field($model, 'actors')->textInput() ?>

    <?= $form->field($model, 'director')->textInput() ?>

    <?= $form->field($model, 'producer')->textInput() ?>

    <?= $form->field($model, 'thumb')->fileInput() ?>

    <?= $form->field($model, 'sumary')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'details')->textarea(['rows' => 6]) ?>

    <?php
        echo $form->field($model, 'type')->dropdownList(
            FilmType::find()->select(['name', 'id'])->indexBy('id')->column(),
            ['prompt'=>'Chọn loại phim']
        );
    ?>

    <?= $form->field($model, 'status')->dropdownList(
        ['1' => 'Hiển thị', '0' => 'Ẩn']
    ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Thêm' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
