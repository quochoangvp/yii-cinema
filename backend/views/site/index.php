<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Tổng quan';
?>
<div class="row home-widgets">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Rạp', Url::to('cinema'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý thông tin các rạp chiếu phim</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Phim', Url::to('film'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý thông tin các phim đang chiếu</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Thể loại phim', Url::to('film_cate'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý danh sách các thể loại phim</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Lịch chiếu phim', Url::to('schedule'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý lịch chiếu phim</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Vé', Url::to('ticket'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý danh sách các vé xem phim</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
        <div class="panel panel-default">
            <div class="panel-heading"><?= Html::a('Thành viên', Url::to('user'), ['class' => 'td-n']) ?></div>
            <div class="panel-body">Quản lý thông tin các thành viên của website</div>
        </div>
    </div>
</div>