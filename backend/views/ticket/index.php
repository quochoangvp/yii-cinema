<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Danh sách vé';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id="w0" class="grid-view">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Phim</th>
                    <th>Loại phim</th>
                    <th>Rạp</th>
                    <th>Khung giờ</th>
                    <th>Khách hàng</th>
                    <th>Số lượng vé</th>
                    <th>Trạng thái</th>
                    <th class="action-column">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($schedules as $schedule): ?>
                <tr data-key="<?= $schedule['id'] ?>">
                    <td><?= $schedule['film_name'] ?></td>
                    <td><?= $schedule['film_type'] ?></td>
                    <td><?= $schedule['cinema_name'] ?></td>
                    <td><?= $schedule['time'] ?></td>
                    <td><?= $schedule['fullname'] ?></td>
                    <td><?= $schedule['ticket_qty'] ?></td>
                    <td><?= ($schedule['status'] == 1) ? 'Đã duyệt' : 'Chưa duyệt' ?></td>
                    <td>
                        <a href="javascript:;" title="Duyệt" aria-label="Duyệt" data-pjax="<?= $schedule['id'] ?>"><span class="glyphicon glyphicon-ok"></span></a>
                    </td>
                </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>
