<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách vé', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'schedule_id',
            'user_id',
            'film_type',
            'ticket_qty',
            'status',
        ],
    ]) ?>

</div>
