<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FilmType */

$this->title = 'Thêm mới loại phim';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách loại phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
