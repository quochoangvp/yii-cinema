<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilmType */

$this->title = 'Sửa loại phim: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Danh sách loại phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Sửa';
?>
<div class="film-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
