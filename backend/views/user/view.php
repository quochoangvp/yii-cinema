<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Thành viên', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Chỉnh sửa', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn chắc chắn muốn xóa thành viên này?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'username',
            'fullname',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            'mobile',
            'address',
            [
                'attribute' => 'status',
                'value' => ($model->status == 10) ? 'Hoạt động' : 'Không hoạt động'
            ],
            [
                'attribute' => 'role',
                'value' => ($model->role == 20) ? 'Quản trị viên' : 'Thành viên'
            ],
            [
                'attribute' => 'created_at',
                'value' => date('Y-d-m H:j:s', $model->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => date('Y-d-m H:j:s', $model->created_at)
            ],
        ],
    ]) ?>

</div>
