<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thành viên';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Thêm thành viên', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'username',
            'fullname',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            'mobile',
            'address',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    return ($model->status == 10) ? 'Hoạt động' : 'Không hoạt động';
                }
            ],
            [
                'attribute' => 'role',
                'value' => function($model)
                {
                    return ($model->role == 20) ? 'Quản trị viên' : 'Thành viên';
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => function($model)
                {
                    return date('d-m-Y', $model->created_at);
                }
            ],
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
