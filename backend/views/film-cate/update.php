<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilmCate */

$this->title = 'Cập nhật thể loại: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Thể loại phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Cập nhật';
?>
<div class="film-cate-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
