<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FilmCate */

$this->title = 'Thêm thể loại phim mới';
$this->params['breadcrumbs'][] = ['label' => 'Danh sách thể loại', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-cate-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
