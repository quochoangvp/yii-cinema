<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FilmCate */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Thể loại phim', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-cate-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cập nhật', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Xóa', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Bạn có chắc chắn muốn xóa?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'slug',
            [
                'attribute' => 'status',
                'value' => ($model->status) ? 'Hiển thị' : 'Ẩn'
            ]
        ],
    ]) ?>

</div>
