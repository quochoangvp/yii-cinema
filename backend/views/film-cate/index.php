<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\FilmCateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Thể loại phim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-cate-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Thêm thể loại mới', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'slug',
            [
                'attribute' => 'status',
                'value' => function($model)
                {
                    if ($model->status === 1) {
                        return 'Hiển thị';
                    } else {
                        return 'Ẩn';
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
