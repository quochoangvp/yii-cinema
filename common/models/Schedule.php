<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "schedule".
 *
 * @property integer $id
 * @property integer $film_id
 * @property integer $cinema_id
 * @property string $time
 * @property integer $status
 */
class Schedule extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['film_id', 'cinema_id', 'time'], 'required'],
            [['film_id', 'cinema_id', 'status'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'film_id' => 'Phim',
            'cinema_id' => 'Rạp',
            'time' => 'Thời gian',
            'status' => 'Trạng thái',
        ];
    }

    public function getFilms()
    {
        return $this->hasMany(Film::className(), ['cate_id' => 'id']);
    }

    public function getCinemas()
    {
        return $this->hasOne(Cinema::className(), ['id' => 'cinema_id']);
    }

    public function getDateByCinemaAndFilm($film_id, $cinema_id)
    {
        return Schedule::find()->select('id, time')->where(['film_id' => $film_id, 'cinema_id' => $cinema_id, 'status' => 1])->asArray()->all();
    }

    public function getTimeByDate($date)
    {
        $date = date_format(date_create($date), 'Y-m-d');
        $query = new \yii\db\Query();
        $query
            ->select('schedule.*')
            ->from('schedule')
            ->where(['like', 'time', $date]);
        $command = $query->createCommand();
        $rows = $command->queryAll();
        return $rows;
    }

    public function getScheduleDetails($id)
    {
        $query = new \yii\db\Query();
        $query
            ->select('`schedule`.`id`, `film`.`name` AS `film_name`, `film`.`id` AS `film_id`, `film`.`thumb`, `schedule`.`time`, `cinema`.`name` AS `cinema_name`, `cinema`.`id` AS `cinema_id`,
                    `film_type`.`name` AS `film_type_name`, `film_type`.`id` AS `film_type_id`')
            ->from('`schedule`')
            ->leftJoin('`cinema`', '`schedule`.`cinema_id` = `cinema`.`id`')
            ->leftJoin('`film`', '`schedule`.`film_id` = `film`.`id`')
            ->leftJoin('`film_type`', '`film`.`type` = `film_type`.`id`')
            ->where(['`schedule`.`id`' => $id]);
        return $query->one();
    }
}
