<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $thumbnail
 * @property string $created_at
 * @property string $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'body'], 'required'],
            [['body'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['thumbnail'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề',
            'body' => 'Nội dung',
            'thumbnail' => 'Ảnh đại diện',
            'created_at' => 'Ngày đăng',
            'updated_at' => 'Cập nhật cuối',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            if ($this->thumbnail && is_object($this->thumbnail)) {
                $this->thumbnail->saveAs(Yii::getAlias('@frontend/web/images/uploads/' . $this->thumbnail->baseName . '.' . $this->thumbnail->extension));
            }
            return true;
        } else {
            return false;
        }
    }
}
