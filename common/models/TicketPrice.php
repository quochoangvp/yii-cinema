<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket_price".
 *
 * @property string $id
 * @property string $cinema_id
 * @property string $film_type
 * @property string $price
 */
class TicketPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cinema_id', 'film_type', 'price'], 'required'],
            [['cinema_id', 'film_type', 'price'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cinema_id' => 'Phim',
            'film_type' => 'Loại phim',
            'price' => 'Giá',
        ];
    }
}
