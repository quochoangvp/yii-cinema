<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property integer $schedule_id
 * @property string $user_id
 * @property string $film_type
 * @property integer $ticket_qty
 * @property integer $status
 */
class Ticket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['schedule_id', 'user_id', 'film_type', 'status'], 'required'],
            [['schedule_id', 'user_id', 'film_type', 'ticket_qty', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'schedule_id' => 'Lịch',
            'user_id' => 'Khách hàng',
            'film_type' => 'Loại phim',
            'ticket_qty' => 'Số lượng vé',
            'status' => 'Trạng thái',
        ];
    }

    public function getScheduleList()
    {
        $query = new \yii\db\Query();
        $query
            ->select('`schedule`.`id`, `film`.`name` AS `film_name`, `film_type`.`name` AS `film_type`, `cinema`.`name` AS `cinema_name`, `schedule`.`time`, `user`.`fullname`, `ticket`.`ticket_qty`, `ticket`.`status`')
            ->from('`ticket`')
            ->leftJoin('`film_type`', '`ticket`.`film_type` = `film_type`.`id`')
            ->leftJoin('`user`', '`ticket`.`user_id` = `user`.`id`')
            ->leftJoin('`schedule`', '`ticket`.`schedule_id` = `schedule`.`id`')
            ->leftJoin('`film`', '`schedule`.`film_id` = `film`.`id`')
            ->leftJoin('`cinema`', '`schedule`.`cinema_id` = `cinema`.`id`');
        return $query->all();
    }
}
