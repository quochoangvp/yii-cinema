<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cinema".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $image
 * @property string $slot
 */
class Cinema extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cinema';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'slot'], 'required'],
            [['name'], 'string', 'max' => 30],
            [['address'], 'string', 'max' => 255],
            [['slot'], 'integer'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'address' => 'Địa chỉ',
            'slot' => 'Số ghế',
            'image' => 'Hình ảnh'
        ];
    }

    public function getCinemaByFilmId($film_id)
    {
        $query = new \yii\db\Query();
        $query
            ->select('`cinema`.*')
            ->from('`cinema`')
            ->innerJoin('`schedule`', '`cinema`.`id` = `schedule`.`cinema_id`')
            ->where(['`schedule`.`film_id`' => $film_id])
            ->groupBy('`schedule`.`id`');
        $command = $query->createCommand();
        $rows = $command->queryAll();
        return $rows;
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->image->saveAs(Yii::getAlias('@frontend/web/images/uploads/' . $this->image->baseName . '.' . $this->image->extension));
            return true;
        } else {
            return false;
        }
    }
}
