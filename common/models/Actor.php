<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "actor".
 *
 * @property integer $id
 * @property string $fullname
 * @property string $birthday
 * @property integer $height
 * @property integer $weight
 * @property string $national
 * @property string $bio
 * @property integer $status
 */
class Actor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'birthday', 'height', 'weight', 'national', 'bio'], 'required'],
            [['birthday'], 'safe'],
            [['height', 'weight', 'status'], 'integer'],
            [['bio'], 'string'],
            [['fullname'], 'string', 'max' => 60],
            [['national'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Họ tên',
            'birthday' => 'Ngày sinh',
            'height' => 'Chiều cao',
            'weight' => 'Cân nặng',
            'national' => 'Quốc tịch',
            'bio' => 'Giới thiệu',
            'status' => 'Trạng thái',
        ];
    }
}
