<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "film_cate".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $status
 */
class FilmCate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'film_cate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 60],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'slug' => 'Đường dẫn',
            'status' => 'Trạng thái',
        ];
    }

    public function getFilms()
    {
        return $this->hasMany(Film::className(), ['cate_id' => 'id']);
    }
}
