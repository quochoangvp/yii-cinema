<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "film".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $start
 * @property string $end
 * @property integer $cate_id
 * @property integer $actors
 * @property integer $director
 * @property integer $producer
 * @property string $thumb
 * @property string $sumary
 * @property string $details
 * @property string $trailer_link
 * @property integer $type
 * @property integer $status
 */
class Film extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'film';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'start', 'end', 'cate_id', 'actors', 'director', 'producer', 'sumary', 'details', 'type'], 'required'],
            [['start', 'end'], 'safe'],
            [['cate_id', 'type', 'status'], 'integer'],
            [['sumary', 'details', 'actors', 'director', 'producer'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['trailer_link'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
            [['thumb'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Tên',
            'slug' => 'Đường dẫn',
            'start' => 'Khởi chiếu',
            'end' => 'Kết thúc',
            'cate_id' => 'Thể loại',
            'actors' => 'Diễn viên',
            'director' => 'Đạo diễn',
            'producer' => 'Nhà sản xuất',
            'thumb' => 'Ảnh đại diện',
            'sumary' => 'Mô tả ngắn',
            'details' => 'Chi tiết',
            'trailer_link' => 'Trailer',
            'type' => 'Loại phim',
            'status' => 'Trạng thái',
        ];
    }

    public function getFilmCate()
    {
        return $this->hasOne(FilmCate::className(), ['id' => 'cate_id']);
    }

    public static function getFilmScreened($whereArr = [], $limit = 10)
    {

        $where = [];

        if (is_array($whereArr) && count($whereArr) > 0) {
            $where = array_merge($where, $whereArr);
        }

        $filmScreened = Film::find()
            ->select('film.*, film_cate.name as cate_name')
            ->leftJoin('film_cate', '`film_cate`.`id` = `film`.`cate_id`')
            ->where($where)
            ->with('filmCate')
            ->limit($limit)
            ->orderBy([
                'start' => SORT_DESC
            ])
            ->all();
        return $filmScreened;
    }

    public static function searchFilmByName($keyword)
    {
        if (strlen($keyword) > 0) {

            return Film::find()
                ->select('film.*, film_cate.name as cate_name')
                ->with('filmCate')
                ->leftJoin('film_cate', '`film_cate`.`id` = `film`.`cate_id`')
                ->where('film.name like "%' . $keyword . '%"')
                ->orderBy([
                    'start' => SORT_DESC
                ])->all();
        }
    }

    public static function getBySlug($slug)
    {
        return Film::findOne(['slug' => $slug]);
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->thumb->saveAs(Yii::getAlias('@frontend/web/images/uploads/' . $this->thumb->baseName . '.' . $this->thumb->extension));
            return true;
        } else {
            return false;
        }
    }
}
