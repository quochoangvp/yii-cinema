-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2016 at 08:39 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cinema`
--
CREATE DATABASE IF NOT EXISTS `cinema` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `cinema`;

-- --------------------------------------------------------

--
-- Table structure for table `cinema`
--

DROP TABLE IF EXISTS `cinema`;
CREATE TABLE `cinema` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `slot` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `cinema`
--

TRUNCATE TABLE `cinema`;
--
-- Dumping data for table `cinema`
--

INSERT INTO `cinema` (`id`, `name`, `address`, `image`, `slot`) VALUES
(1, 'Galaxy Nguyễn Du', '116 Nguyễn Du, Quận 1, Tp.HCM', '2 nd 2511.jpg', 100),
(2, 'Galaxy Nguyễn Trãi', '230 Nguyễn Trãi, Quận 1, Tp.HCM', 'co nt 2.jpg', 120),
(3, 'Galaxy Tân Bình', '246 Nguyễn Hồng Đào, Q.TB, Tp.HCM', 'tb 1.jpg', 160),
(4, 'Galaxy Kinh Dương Vương', '718bis Kinh Dương Vương, Q6, TpHCM', 'kdv 1.jpg', 200),
(5, 'Galaxy Bến Tre', 'Lầu 1, Sense City 26A Trần Quốc Tuấn, Phường 4, TP. Bến Tre', '4_12.jpg', 140);

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `cate_id` int(11) NOT NULL,
  `actors` text NOT NULL,
  `producer` varchar(30) NOT NULL,
  `director` varchar(30) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `sumary` text NOT NULL,
  `details` text NOT NULL,
  `trailer_link` varchar(255) DEFAULT NULL,
  `type` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `film`
--

TRUNCATE TABLE `film`;
--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `name`, `slug`, `start`, `end`, `cate_id`, `actors`, `producer`, `director`, `thumb`, `sumary`, `details`, `trailer_link`, `type`, `status`) VALUES
(1, 'Batman Đại Chiến Superman: Ánh Sáng Công Lý', 'batman-vs-superman', '2016-03-01', '2016-04-09', 3, 'Batman, Superman', 'Batman', 'Superman', '', 'Batman Đại Chiến Superman là cuộc đối đầu có 1 không 2 của 2 siêu anh hùng mạnh mẽ đáng gờm của thành phố Gotham với biểu tượng được tôn sùng nhất từ thành phố Metropolis.', 'Batman Đại Chiến Superman là cuộc đối đầu có 1 không 2 của 2 siêu anh hùng mạnh mẽ đáng gờm của thành phố Gotham với biểu tượng được tôn sùng nhất từ thành phố Metropolis. Cuộc chiến bắt nguồn từ việc họ đang lo lắng vì không thể kiểm soát được các siêu anh hùng mới có sức mạnh thần thánh và dẫn đến bất đồng quan điểm. Tuy nhiên, trong khi Batman đại chiến Superman nhiều trận liền thì một mối đe dọa khác đã nổi lên, có nguy cơ đẩy nhân loại vào tình thế nguy hiểm hơn bao giờ hết.', 'https://www.youtube.com/embed/PWcimiuIGxM', 2, 1),
(2, 'Midnight Special / Nhãn Lực Siêu Nhiên', 'midnight-special-nhan-luc-sieu-nhien', '2016-04-01', '2016-04-15', 2, '', '', '', 'midnight-special-vnese-poster.jpg', 'Được dàn dựng theo phong cách phim khoa học viễn tưởng thập niên 90, cậu bé Alton sở hữu năng lực siêu phàm khiến bản thân bị hai thế lực tranh giành. Một là hội kín bên coi cậu là thánh thần, tôn sùng từng lời nói.', 'Được dàn dựng theo phong cách phim khoa học viễn tưởng thập niên 90, cậu bé Alton sở hữu năng lực siêu phàm khiến bản thân bị hai thế lực tranh giành. Một là hội kín bên coi cậu là thánh thần, tôn sùng từng lời nói. Một bên là chính phủ tìm mọi cách săn lùng cậu. Thế nhưng, mong ước lớn nhất của cha Alton là con trai mình có thể yên ổn lớn lên…\r\n\r\nVới sự góp mặt của các ngôi sao thực lực như Michael Shannon, Kirsten Dunst và diễn xuất xúc động của Jaeden Lieberher trong vai cậu bé Alton, phim hứa hẹn sẽ bùng nổ các rạp trong thời gian tới.', NULL, 1, 1),
(3, 'Kung Fu Panda 3', 'kungfu-panda-3', '2016-03-12', '2016-04-07', 1, '', '', '', 'kungfu3.jpg', 'Trong Kungfu Panda 3, Po gặp lại người cha thất lạc lâu năm, và được đưa đến ngôi làng quê hương, nơi cậu gặp rất nhiều đồng loại. Tuy nhiên, Po có vẻ không hợp lắm, bởi đã quen với cuộc sống cũ.', 'Trong Kungfu Panda 3, Po gặp lại người cha thất lạc lâu năm, và được đưa đến ngôi làng quê hương, nơi cậu gặp rất nhiều đồng loại. Tuy nhiên, Po có vẻ không hợp lắm, bởi đã quen với cuộc sống cũ. Khi một linh hồn quỷ dữ tên Kai bắt đầu kế hoạch xâm chiếm Trung Hoa, Po đành phải tập hợp cả làng lại và huấn luyện họ thành đội quân chiến đấu, vốn là điều không hề dễ dàng. Kungfu Panda 3 vẫn có phần lồng tiếng của các ngôi sao cũ như Black Jack, Angelina Jolie, Dustin Hoffman.', NULL, 1, 1),
(4, 'The Boy - Cậu Bé Ma', 'the-boy-cau-be-ma', '2016-03-11', '2016-04-07', 3, '', '', '', 'the-boy-cau-be-ma.jpg', 'Cậu Bé Ma xoay quanh Greta (Lauren Cohan) - một cô gái xinh đẹp nhận công việc chăm sóc cậu con trai 8 tuổi của cặp vợ chồng già người Anh. Cô đã bất ngờ khi biết rằng cậu bé Brahms đó chỉ là một con búp bê bằng gỗ nhưng lại được đối xử như người sống.', 'Là hiện thân của đứa trẻ đã qua đời cách đó 20 năm, Brahms có một danh sách dài những quy tắc mà Greta buộc phải tuân thủ theo. Khi cặp vợ chồng già lên đường đi nghỉ mát, Greta chỉ còn đơn độc một mình trong căn biệt thự rộng lớn. Cô làm ngơ những quy tắc và trở nên thích thú với những câu nói tán tình của anh chàng giao hàng đẹp trai Malcolm (Rupert Evans), cho tới khi hàng loạt những sự việc kỳ lạ, đáng lo xảy ra khiến cô tin rằng có một sức mạnh siêu nhiên nào đó tồn tại trong ngôi nhà. Greta bắt đầu đi sâu tìm hiểu những điều bí ẩn ghê sợ mà cô phải đối mặt.\r\n\r\nVới những bất ngờ ở bối cảnh vô cùng kinh dị, kịch tính, bộ phim sẽ khiến khán giả chìm trong sự tò mò cho đến những giây phút cuối cùng.', NULL, 2, 1),
(5, 'Kung Fu Panda 3', 'kungfu-panda-3-2', '2016-03-12', '2016-04-07', 1, '', '', '', 'kungfu3.jpg', 'Trong Kungfu Panda 3, Po gặp lại người cha thất lạc lâu năm, và được đưa đến ngôi làng quê hương, nơi cậu gặp rất nhiều đồng loại. Tuy nhiên, Po có vẻ không hợp lắm, bởi đã quen với cuộc sống cũ.', 'Trong Kungfu Panda 3, Po gặp lại người cha thất lạc lâu năm, và được đưa đến ngôi làng quê hương, nơi cậu gặp rất nhiều đồng loại. Tuy nhiên, Po có vẻ không hợp lắm, bởi đã quen với cuộc sống cũ. Khi một linh hồn quỷ dữ tên Kai bắt đầu kế hoạch xâm chiếm Trung Hoa, Po đành phải tập hợp cả làng lại và huấn luyện họ thành đội quân chiến đấu, vốn là điều không hề dễ dàng. Kungfu Panda 3 vẫn có phần lồng tiếng của các ngôi sao cũ như Black Jack, Angelina Jolie, Dustin Hoffman.', NULL, 1, 1),
(6, 'The Boy - Cậu Bé Ma', 'the-boy-cau-be-ma-2', '2016-03-11', '2016-04-07', 3, '', '', '', 'the-boy-cau-be-ma.jpg', 'Cậu Bé Ma xoay quanh Greta (Lauren Cohan) - một cô gái xinh đẹp nhận công việc chăm sóc cậu con trai 8 tuổi của cặp vợ chồng già người Anh. Cô đã bất ngờ khi biết rằng cậu bé Brahms đó chỉ là một con búp bê bằng gỗ nhưng lại được đối xử như người sống.', 'Là hiện thân của đứa trẻ đã qua đời cách đó 20 năm, Brahms có một danh sách dài những quy tắc mà Greta buộc phải tuân thủ theo. Khi cặp vợ chồng già lên đường đi nghỉ mát, Greta chỉ còn đơn độc một mình trong căn biệt thự rộng lớn. Cô làm ngơ những quy tắc và trở nên thích thú với những câu nói tán tình của anh chàng giao hàng đẹp trai Malcolm (Rupert Evans), cho tới khi hàng loạt những sự việc kỳ lạ, đáng lo xảy ra khiến cô tin rằng có một sức mạnh siêu nhiên nào đó tồn tại trong ngôi nhà. Greta bắt đầu đi sâu tìm hiểu những điều bí ẩn ghê sợ mà cô phải đối mặt.\r\n\r\nVới những bất ngờ ở bối cảnh vô cùng kinh dị, kịch tính, bộ phim sẽ khiến khán giả chìm trong sự tò mò cho đến những giây phút cuối cùng.', NULL, 1, 1),
(7, 'Midnight Special / Nhãn Lực Siêu Nhiên', 'midnight-special-nhan-luc-sieu-nhien-2', '2016-04-01', '2016-04-15', 2, '', '', '', 'midnight-special-vnese-poster.jpg', 'Được dàn dựng theo phong cách phim khoa học viễn tưởng thập niên 90, cậu bé Alton sở hữu năng lực siêu phàm khiến bản thân bị hai thế lực tranh giành. Một là hội kín bên coi cậu là thánh thần, tôn sùng từng lời nói.', 'Được dàn dựng theo phong cách phim khoa học viễn tưởng thập niên 90, cậu bé Alton sở hữu năng lực siêu phàm khiến bản thân bị hai thế lực tranh giành. Một là hội kín bên coi cậu là thánh thần, tôn sùng từng lời nói. Một bên là chính phủ tìm mọi cách săn lùng cậu. Thế nhưng, mong ước lớn nhất của cha Alton là con trai mình có thể yên ổn lớn lên…\r\n\r\nVới sự góp mặt của các ngôi sao thực lực như Michael Shannon, Kirsten Dunst và diễn xuất xúc động của Jaeden Lieberher trong vai cậu bé Alton, phim hứa hẹn sẽ bùng nổ các rạp trong thời gian tới.', NULL, 1, 1),
(8, 'Batman Đại Chiến Superman: Ánh Sáng Công Lý', 'batman-vs-superman-2', '2016-03-25', '2016-04-09', 1, '', '', '', 'batvssu.jpg', 'Batman Đại Chiến Superman là cuộc đối đầu có 1 không 2 của 2 siêu anh hùng mạnh mẽ đáng gờm của thành phố Gotham với biểu tượng được tôn sùng nhất từ thành phố Metropolis.', 'Batman Đại Chiến Superman là cuộc đối đầu có 1 không 2 của 2 siêu anh hùng mạnh mẽ đáng gờm của thành phố Gotham với biểu tượng được tôn sùng nhất từ thành phố Metropolis. Cuộc chiến bắt nguồn từ việc họ đang lo lắng vì không thể kiểm soát được các siêu anh hùng mới có sức mạnh thần thánh và dẫn đến bất đồng quan điểm. Tuy nhiên, trong khi Batman đại chiến Superman nhiều trận liền thì một mối đe dọa khác đã nổi lên, có nguy cơ đẩy nhân loại vào tình thế nguy hiểm hơn bao giờ hết.', NULL, 1, 1),
(9, 'The Jungle Book - Cậu Bé Rừng Xanh', 'the-jungle-book', '2016-04-14', '2016-05-18', 1, 'Neel Sethi, Ben Kingsley, Lupita Nyong''o, Giancarlo Esposito, Scarlett Johansson, Idris Elba', 'Disney', 'Jon Favreau', 'the jungle book_1.jpg', 'Bộ phim Cậu Bé Rừng Xanh, phiên bản người đóng hoàn toàn mới mẻ về hành trình kỳ thú của Mowgli (Neel Sethi thủ vai) - một cậu bé mồ côi được nuôi nấng bởi đàn chó sói.', 'Bộ phim Cậu Bé Rừng Xanh, phiên bản người đóng hoàn toàn mới mẻ về hành trình kỳ thú của Mowgli (Neel Sethi thủ vai) - một cậu bé mồ côi được nuôi nấng bởi đàn chó sói. Nhưng Mowgli nhận ra mình không còn được chào đón ở ngôi nhà rừng già khi con hổ đáng sợ Shere Khan (Idris Elba lồng tiếng) – kẻ phải mang đầy những vết sẹo do loài người gây ra – tuyên bố sẽ tiêu diệt những gì hắn cho là hiểm họa. Buộc phải rời bỏ nơi dung thân duy nhất, Mowgli bắt đầu chuyến hành trình tự khám phá với sự chỉ dẫn của báo đen nghiêm nghị Bagheera (Ben Kingsley lồng tiếng), và gấu Baloo (Bill Murray). Trên đường đi, Mowgli phải đối mặt với những sinh vật xấu xa của rừng già, trong đó có trăn Kaa (Scarlett Johansson lồng tiếng) với giọng nói mê hoặc và khả năng thôi miên con người, khỉ đột biết tiếng người King Louie (Christopher Walken lồng tiếng). Chúng cố ép buộc Mowgli phải tiết lộ bí mật tạo ra “bông hoa đỏ” lập lòe đầy nguy hiểm: Lửa.', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `film_cate`
--

DROP TABLE IF EXISTS `film_cate`;
CREATE TABLE `film_cate` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `film_cate`
--

TRUNCATE TABLE `film_cate`;
--
-- Dumping data for table `film_cate`
--

INSERT INTO `film_cate` (`id`, `name`, `slug`, `status`) VALUES
(1, 'Hành động', 'hanh-dong', 0),
(2, 'Tình cảm', 'tinh-cam', 1),
(3, 'Viễn tưởng', 'vien-tuong', 1),
(4, 'Phiêu lưu', 'phieu-luu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `film_type`
--

DROP TABLE IF EXISTS `film_type`;
CREATE TABLE `film_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `film_type`
--

TRUNCATE TABLE `film_type`;
--
-- Dumping data for table `film_type`
--

INSERT INTO `film_type` (`id`, `name`) VALUES
(1, '2D'),
(2, '3D');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `news`
--

TRUNCATE TABLE `news`;
--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `body`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 'Angela Phương Trinh bất ngờ có mặt trong chiến dịch quảng cáo D&G', '<p class="lead">Người đẹp nhận được lời mời g&oacute;p mặt v&agrave;o chiến dịch lưu giữ những khoảnh khắc đẹp tại LHP Cannes của nh&agrave; mốt danh tiếng.&nbsp;</p>\r\n<div class="reference_news width_common">&nbsp;</div>\r\n<div class="fck_detail width_common">\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/1-9744-1463886239.jpg" alt="angela-phuong-trinh-bat-ngo-co-mat-trong-chien-dich-quang-cao-dg" data-width="500" data-pwidth="637" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Angela Phương Trinh g&oacute;p mặt trong chiến dịch quảng c&aacute;o ghi lại những khoảnh khắc đẹp tại LHP Cannes&nbsp;của nh&agrave; mốt danh tiếng Dolce &amp; Gabbana. Ảnh chụp m&agrave;n h&igrave;nh.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Người đẹp chia sẻ, khi c&ocirc; đang tạo d&aacute;ng với bộ v&aacute;y đại dương của NTK C&ocirc;ng Tr&iacute;, &ecirc; k&iacute;p chụp h&igrave;nh đến từ Dolce&amp;Gabbana đ&atilde; ngỏ lời mời c&ocirc; tham gia v&agrave;o chiến dịch quảng c&aacute;o của họ. Đ&acirc;y l&agrave; chiến dịch lưu giữ những khoảnh khắc đẹp tại Cannes của nh&agrave; mốt lừng danh n&agrave;y.&nbsp;</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/2-2530-1463886239.jpg" alt="angela-phuong-trinh-bat-ngo-co-mat-trong-chien-dich-quang-cao-dg-1" data-natural-width="600" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Angela Phương Trinh g&oacute;p mặt trong shoot h&igrave;nh của Dolce&amp;Gabbana. Ảnh chụp m&agrave;n h&igrave;nh.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Trong shoot h&igrave;nh của Dolce&amp;Gabbana, Angela Phương Trinh mặc chiếc v&aacute;y chủ để đại dương của NTK C&ocirc;ng Tr&iacute;, sử dụng khăn đội đầu v&agrave; phụ kiện chiếc m&aacute;y ảnh cầm tay của nh&agrave; mốt n&agrave;y. Người đẹp rất hạnh ph&uacute;c với trải nghiệm tuyệt vời n&agrave;y v&agrave; hứa hẹn sẽ chia sẻ nhiều hơn sau khi trở về Việt Nam.&nbsp;</p>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/6-5184-1463887831.jpg" alt="Mỗi bức h&igrave;nh lưu giữ một khoảnh khắc, một" data-natural-width="600" data-width="600" data-pwidth="637" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Chiến dịch quảng c&aacute;o&nbsp;của nh&agrave; mốt Dolce&amp;Gabbana gồm một chuỗi h&igrave;nh ảnh lưu giữ những khoảnh khắc đẹp tại LHP Cannes, mỗi h&igrave;nh ảnh lại mang một chủ đề, quy luật kh&aacute;c nhau, từ thảm đỏ, buổi tiệc cho tới chủ đề h&egrave; phố. Angela Phương Trinh l&agrave; một trong những vị kh&aacute;ch được mời g&oacute;p mặt v&agrave;o bức h&igrave;nh thứ 12 với chủ đề "Surround yourself with stylish&nbsp;friends" (Tạm dịch: Toả s&aacute;ng c&ugrave;ng những người bạn s&agrave;nh điệu).&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/8-8542-1463887831.jpg" alt="Chia sẻ về trải nghiệm th&uacute; vị n&agrave;y, Angela Phương Trinh cho biết, h&ocirc;m 18/5, khi c&ocirc; c&ugrave;ng nhiếp ảnh gia Anh Huy Phạm đang thực hiện v&agrave;i bức ảnh kỷ niệm tr&ecirc;n bờ biển Cannes, th&igrave; nhận được lời mời của &ecirc;kip Dolce&amp;Gabbana. &Ecirc;kip cho biết, họ ấn tượng với gương mặt, phong c&aacute;ch của Phương Trinh v&agrave; bộ v&aacute;y biển cả của NTK C&ocirc;ng Tr&iacute;, n&ecirc;n muốn mời c&ocirc; tham gia buổi chụp ảnh." data-natural-width="600" data-width="600" data-pwidth="637" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Chia sẻ về trải nghiệm th&uacute; vị n&agrave;y, Angela Phương Trinh cho biết, h&ocirc;m 18/5, khi c&ocirc; c&ugrave;ng nhiếp ảnh gia Anh Huy Phạm đang thực hiện v&agrave;i bức ảnh kỷ niệm tr&ecirc;n bờ biển Cannes&nbsp;th&igrave; nhận được lời mời của &ecirc;kip Dolce&amp;Gabbana. &Ecirc;kip cho biết, họ ấn tượng với gương mặt, phong c&aacute;ch của Phương Trinh v&agrave; bộ v&aacute;y biển cả của NTK C&ocirc;ng Tr&iacute;&nbsp;n&ecirc;n muốn mời c&ocirc; tham gia buổi chụp ảnh.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/9-2834-1463887831.jpg" alt="Khi được chia sẻ r&otilde; về campaign thời trang th&uacute; vị n&agrave;y, nữ diễn vi&ecirc;n" data-natural-width="600" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Khi được chia sẻ r&otilde; về campaign thời trang th&uacute; vị n&agrave;y, nữ diễn vi&ecirc;n "Sứ mệnh tr&aacute;i tim" đ&atilde; rất vui vẻ nhận lời. Sau buổi chụp h&igrave;nh, nh&agrave; mốt&nbsp;Dolce&amp;Gabbana đ&atilde; th&ocirc;ng b&aacute;o với Phương Trinh bức ảnh sẽ được đăng tải v&agrave;o cuối tuần, k&egrave;m một bản thoả thuận sử dụng h&igrave;nh ảnh rất chuy&ecirc;n nghiệp.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/10-8911-1463887831.jpg" alt="Buổi chụp ảnh được thực hiện ngay tr&ecirc;n bờ biển xinh đẹp của Cannes v&agrave; mọi người kết hợp c&ugrave;ng nhau rất ăn &yacute;." data-natural-width="600" data-width="600" data-pwidth="637" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">Buổi chụp ảnh được thực hiện ngay tr&ecirc;n bờ biển xinh đẹp&nbsp;v&agrave; mọi người kết hợp c&ugrave;ng nhau rất ăn &yacute;.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="tplCaption" border="0" cellspacing="0" cellpadding="3" align="center">\r\n<tbody>\r\n<tr>\r\n<td><img src="http://img.f21.ngoisao.vnecdn.net/2016/05/22/11-9999-1463887831.jpg" alt="&aacute;c người mẫu của Dolce&amp;Gabbana tỏ ra rất h&agrave;o hứng. Họ hỏi c&ocirc; về c&ocirc;ng việc v&agrave; d&agrave;nh tặng nhiều lời khen về phụ nữ v&agrave; đất nước Việt Nam. Phương Trinh v&agrave; nh&oacute;m người mẫu kh&ocirc;ng qu&ecirc;n chụp ảnh" data-natural-width="600" data-width="600" data-pwidth="637" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p class="Image">C&aacute;c người mẫu của Dolce&amp;Gabbana tỏ ra rất h&agrave;o hứng với người đẹp Việt. Họ hỏi c&ocirc; về c&ocirc;ng việc v&agrave; d&agrave;nh tặng nhiều lời khen về phụ nữ v&agrave; đất nước Việt Nam. Angela Phương Trinh v&agrave; nh&oacute;m người mẫu kh&ocirc;ng qu&ecirc;n chụp ảnh "tự sướng" trước khi chia tay.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><strong>Vi&ecirc;n Vi&ecirc;n</strong><br /><em>Ảnh: </em><strong>Anh Huy Phạm</strong></p>\r\n</div>', '1-9744-1463886239.jpg', '2016-05-22 11:22:39', '2016-05-22 11:22:39'),
(2, 'Tuần lễ phim Việt chính thức diễn ra trên toàn quốc', '<p><strong>&nbsp;<a title="Tuần lễ phim Việt" href="http://www.vietgiaitri.com/tag/tuan-le-phim-viet/" target="_blank">Tuần lễ phim Việt</a> vừa được khai mạc tại H&agrave; Nội với sự tham dự của đ&ocirc;ng đảo c&aacute;c nghệ sĩ: diễn vi&ecirc;n ChiPu, diễn vi&ecirc;n Thuỳ Anh, Hot girl Salim, Ha Min, nh&oacute;m O-Plus.</strong></p>\r\n<p>Sau buổi khai mạc tại TP HCM v&agrave;o ng&agrave;y 20/5 vừa qua, <a title="Tuần lễ phim Việt" href="http://www.vietgiaitri.com/tag/tuan-le-phim-viet/" target="_blank">Tuần lễ phim Việt</a> ch&iacute;nh thức mở m&agrave;n tại H&agrave; Nội v&agrave;o ng&agrave;y 22/5 v&agrave; nhận đươc sự đ&oacute;n nhận nồng nhiệt từ giới truyền th&ocirc;ng v&agrave; kh&aacute;n giả trẻ.</p>\r\n<p>Điểm nhấn độc đ&aacute;o Tuần lễ phim Việt l&agrave; Cinema tour &ndash; nơi c&ocirc;ng ch&uacute;ng y&ecirc;u phim c&oacute; thể giao lưu với những nghệ nổi tiếng. L&agrave; kh&aacute;ch mời danh dự trong sự kiện ng&agrave;y 22/5, Chipu cho biết Tuần lễ <a title="Phim Việt" href="http://www.vietgiaitri.com/tag/phim-viet/" target="_blank">Phim Việt</a> sẽ l&agrave; một trong những động lực th&ocirc;i th&uacute;c c&aacute;c nh&agrave; sản xuất phim tr&igrave;nh l&agrave;ng những t&aacute;c phẩm <a title="điện ảnh" href="http://www.vietgiaitri.com/category/dien-anh/" target="_blank">điện ảnh</a> mới chất lượng hơn nữa.</p>\r\n<p><img id="6cc" src="http://img.tintuc.vietgiaitri.com/2016/5/24/tuan-le-phim-viet-chinh-thuc-dien-ra-tren-toan-quoc-6926cc.jpg" alt="Tuần lễ phim Việt ch&iacute;nh thức diễn ra tr&ecirc;n to&agrave;n quốc" /></p>\r\n<div>&nbsp;</div>\r\n<div id="LeftPost">&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<p>Tuần lễ phim Việt thu h&uacute;t sự quan t&acirc;m của đ&ocirc;ng đảo kh&aacute;n giả. Ảnh:&nbsp;<strong>CGV</strong></p>\r\n<p>&ldquo;L&agrave; một diễn vi&ecirc;n, t&ocirc;i thấy Tuần lễ phim Việt n&agrave;y l&agrave; một hoạt động rất &yacute; nghĩa. Chương tr&igrave;nh kh&ocirc;ng chỉ tạo điều kiện cho phim Việt đến gần hơn với c&aacute;c kh&aacute;n giả trẻ, m&agrave; c&ograve;n l&agrave; dịp để những diễn vi&ecirc;n như ch&uacute;ng t&ocirc;i c&oacute; dịp gặp gỡ, giao lưu với người h&acirc;m mộ. B&ecirc;n cạnh đ&oacute;, c&aacute;c nh&agrave; l&agrave;m phim cũng hiểu hơn gu thưởng thức của kh&aacute;n giả giữa tr&agrave;o lưu phim ngoại đang tr&agrave;n ngập thị trường&rdquo; &ndash; Thuỳ Anh &ndash; nữ diễn vi&ecirc;n được đ&aacute;nh gi&aacute; cao trong phim&nbsp;Đập c&aacute;nh giữa kh&ocirc;ng trung&nbsp;n&oacute;i với b&aacute;o giới.</p>\r\n<p>Tuần lễ phim Việt tại H&agrave; Nội c&ograve;n c&oacute; sự g&oacute;p mặt của nh&oacute;m O-Plus. Thời gian gần đ&acirc;y, boysband n&agrave;y c&oacute; cơ hội được tiếp x&uacute;c gần hơn với <a title="điện ảnh" href="http://www.vietgiaitri.com/category/dien-anh/" target="_blank">điện ảnh</a> qua dự &aacute;n phim ngắn <a title="&acirc;m nhạc" href="http://www.vietgiaitri.com/category/am-nhac/" target="_blank">&acirc;m nhạc</a>&nbsp;Bye Bye&nbsp;mới ra mắt. Đại diện O-Plus cho biết: &ldquo;Qua việc thực hiện phim ca nhạc, ch&uacute;ng t&ocirc;i phần n&agrave;o hiểu được sự kh&oacute; khăn cũng như rất kh&acirc;m phục c&aacute;c nh&agrave; l&agrave;m phim trong nước khi thực hiện một bộ phim chiếu rạp&rdquo;.</p>\r\n<p><img id="bdb" src="http://img.tintuc.vietgiaitri.com/2016/5/24/tuan-le-phim-viet-chinh-thuc-dien-ra-tren-toan-quoc-e5dbdb.jpg" alt="Tuần lễ phim Việt ch&iacute;nh thức diễn ra tr&ecirc;n to&agrave;n quốc" /></p>\r\n<p>"<a title="Bao giờ c&oacute; y&ecirc;u nhau" href="http://www.vietgiaitri.com/tag/bao-gio-co-yeu-nhau/" target="_blank">Bao giờ c&oacute; y&ecirc;u nhau</a>" l&agrave; một trong những phim Việt chiếu trong Tuần lễ phim Việt. Ảnh: ĐPCC</p>\r\n<p>Tuần lễ phim Việt diễn ra từ ng&agrave;y 20/5 đến 31/5 tại tất cả hệ thống rạp tr&ecirc;n to&agrave;n quốc. Đ&acirc;y l&agrave; một trong những hoạt động trọng t&acirc;m nhằm giới thiệu những bộ phim Việt chọn lọc đến nhiều hơn với kh&aacute;n giả cũng như đ&oacute;ng g&oacute;p cho sự ph&aacute;t triển của nền c&ocirc;ng nghiệp điện ảnh Việt Nam.</p>\r\n<p>Trong hơn mười ng&agrave;y tổ chức Tuần lễ phim Việt, c&aacute;c kh&aacute;n giả y&ecirc;u phim Việt sẽ c&oacute; cơ hội thưởng thức 7 bộ phim ấn tượng v&agrave; g&acirc;y tiếng vang nhất định tại c&aacute;c ph&ograve;ng chiếu trong thời gian qua như&nbsp;Điệp vụ ch&acirc;n d&agrave;i, G&aacute;i gi&agrave; lắm chi&ecirc;u, Lật mặt, Taxi, em t&ecirc;n g&igrave;, V&ograve;ng eo 56, Bao giờ c&oacute; y&ecirc;u nhauv&agrave;&nbsp;Nữ đại gia&nbsp;với gi&aacute; v&eacute; ưu đ&atilde;i.</p>\r\n<p>Theo Zing</p>', 'tuan-le-phim-viet-chinh-thuc-dien-ra-tren-toan-quoc-6926cc.jpg', '2016-05-24 16:24:47', '2016-05-24 16:24:58'),
(3, 'Phú Đôn: "Không áp lực khi lấy vợ kém 25 tuổi"', '<p>Ph&uacute; Đ&ocirc;n l&agrave; một trong những diễn vi&ecirc;n truyền h&igrave;nh quen mặt với kh&aacute;n giả cả nước. Gương mặt khắc khổ với th&acirc;n h&igrave;nh "c&ograve;m nhom" n&ecirc;n Ph&uacute; Đ&ocirc;n hay được giao những vai kiểu người n&ocirc;ng d&acirc;n chất ph&aacute;c, thật th&agrave;.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-24/1464073854-1464069202-phu-don--3-.jpg" alt="Phỏng vấn Ph&uacute; Đ&ocirc;n - 1" /></p>\r\n<p>Diễn vi&ecirc;n Ph&uacute; Đ&ocirc;n. Ảnh: VFC</p>\r\n<p>Nam diễn vi&ecirc;n ở độ tuổi gần 60 nhưng lại được nhiều người biết đến bằng cuộc h&ocirc;n nh&acirc;n với người vợ trẻ k&eacute;m 25 tuổi. D&ugrave; c&oacute; những c&aacute;ch biệt tuổi t&aacute;c nhưng Ph&uacute; Đ&ocirc;n cho biết bản th&acirc;n thấy kh&ocirc;ng nhiều &aacute;p lực về chuyện kiếm tiền hay ngoại h&igrave;nh.</p>\r\n<p><strong>Sức khỏe k&eacute;m n&ecirc;n kh&ocirc;ng d&aacute;m &ocirc;m đồm nhiều</strong></p>\r\n<p><em>- L&agrave; một c&aacute;i t&ecirc;n quen thuộc tr&ecirc;n truyền h&igrave;nh với nhiều bộ phim nổi tiếng&nbsp;m&agrave; dạo gần đ&acirc;y thấy Ph&uacute; Đ&ocirc;n vắng b&oacute;ng qu&aacute;. Anh bận rộn g&igrave; trong đời sống c&aacute; nh&acirc;n hay hết đam m&ecirc; diễn xuất rồi sao?</em></p>\r\n<p>Đ&uacute;ng l&agrave; gần đ&acirc;y t&ocirc;i kh&ocirc;ng đ&oacute;ng nhiều phim truyền h&igrave;nh lắm lắm. Thứ nhất l&agrave; do tuổi cũng lớn rồi kh&ocirc;ng thể &ocirc;m đồm nhiều như xưa nữa.</p>\r\n<p>Thứ hai l&agrave; xu hướng phim truyền h&igrave;nh b&acirc;y giờ chủ yếu d&agrave;nh cho giới trẻ. C&aacute;c kịch bản cũng chủ yếu xoay quanh đối tượng đ&oacute; n&ecirc;n lượng nh&acirc;n vật lớn tuổi cũng &iacute;t hơn, v&igrave; vậy m&agrave; t&ocirc;i cũng &iacute;t cơ hội diễn hơn trước.</p>\r\n<p>T&ocirc;i cũng c&oacute; tuổi rồi n&ecirc;n trước mỗi kịch bản cũng phải c&acirc;n nhắc nhiều lắm. C&oacute; nhiều kịch bản t&ocirc;i thấy kh&aacute; tốt nhưng sau khi c&acirc;n đo đong đếm về thời gian, sức khỏe v&agrave; nh&acirc;n vật th&igrave; t&ocirc;i kh&ocirc;ng d&aacute;m nhận.</p>\r\n<p><em>- Thế m&agrave; t&ocirc;i cứ tưởng l&agrave; do c&aacute;t-s&ecirc; phim truyền h&igrave;nh thấp qu&aacute; n&ecirc;n anh&hellip; ch&ecirc; cơ đấy?</em></p>\r\n<p>Kh&ocirc;ng phải như thế đ&acirc;u. T&ocirc;i nghĩ rằng, đến thời điểm hiện tại, t&ocirc;i&nbsp; cần phải chắt lọc những vai n&agrave;o ph&ugrave; hợp v&agrave; c&oacute; đủ đất để diễn. Như t&ocirc;i đ&atilde; n&oacute;i đ&oacute;, c&oacute; nhiều kịch bản hay nhưng sau khi c&acirc;n nhắc t&ocirc;i cũng kh&ocirc;ng nhận.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- Anh c&oacute; ngoại h&igrave;nh v&agrave; khả năng diễn xuất kh&aacute; ph&ugrave; hợp với h&agrave;i nhưng c&oacute; vẻ như kh&ocirc;ng mặn m&agrave; lắm với lĩnh vực n&agrave;y?</em></p>\r\n<p>C&oacute; một giai đoạn t&ocirc;i diễn nhiều kh&aacute; nhiều rồi đ&oacute; nhưng về sau, tần suất giảm bớt. Kh&ocirc;ng phải do &aacute;p lực n&agrave;o cả, đơn giản l&agrave; do t&acirc;m l&yacute; của người nghệ sĩ l&uacute;c nắng, l&uacute;c mưa th&ocirc;i.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- Gần đ&acirc;y, t&ocirc;i thấy anh cũng tham gia v&agrave;o nhiều vở diễn cho c&aacute;c em nhỏ trong dịp Tết thiếu nhi. Đối tượng trẻ em khiến anh thay đổi như thế n&agrave;o trong c&aacute;ch diễn xuất, tiết chế tr&ecirc;n s&acirc;n khấu?</em></p>\r\n<p>&nbsp;</p>\r\n<p>L&agrave; một người nghệ sĩ chuy&ecirc;n nghiệp n&ecirc;n với tất cả c&aacute;c vai diễn, t&ocirc;i đều rất h&agrave;o hứng. Nhưng khi diễn cho đối tượng l&agrave; thiếu nhi th&igrave; r&otilde; r&agrave;ng c&oacute; nhiều điều kh&aacute;c biệt với đối tượng kh&aacute;n giả kh&aacute;c. Nh&igrave;n chung, khi được thấy c&aacute;c ch&aacute;u vui với những nụ cười th&igrave; t&ocirc;i cũng c&oacute; cảm gi&aacute;c như được trẻ lại.</p>\r\n<p>Thời gian đầu mới đi diễn, t&ocirc;i cũng c&oacute; nhiều bỡ ngỡ lắm đấy nhưng sau đ&oacute; được anh em đồng nghiệp động vi&ecirc;n n&ecirc;n t&ocirc;i cũng theo được c&aacute;i guồng rất nhanh của c&ocirc;ng việc. Dần dần đến b&acirc;y giờ cũng th&agrave;nh quen rồi.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-24/1464073855-1464069202-phu-don--1-.jpg" alt="Phỏng vấn diễn viễn Ph&uacute; Đ&ocirc;n - 2" /></p>\r\n<p>Nghệ sĩ Ph&uacute; Đ&ocirc;n v&agrave; nghệ sĩ C&ocirc;ng L&yacute; trong một bộ phim.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- Anh thấy c&aacute;t s&ecirc; trong c&aacute;c chương tr&igrave;nh n&agrave;y so với thu nhập của m&igrave;nh như thế n&agrave;o?</em></p>\r\n<p>&nbsp;</p>\r\n<p>Thực ra, những chương tr&igrave;nh h&agrave;i cho thiếu nhi th&igrave; gi&aacute; c&aacute;t-s&ecirc; cũng t&ugrave;y theo số lượng kh&aacute;n giả. Với những chương tr&igrave;nh c&oacute; lượng kh&aacute;n giả tốt th&igrave; anh em cũng c&oacute; thu nhập kh&aacute;. Những với những show m&agrave; kh&aacute;n giả &iacute;t th&igrave; thu nhập cũng thấp hơn, l&uacute;c đ&oacute;, c&aacute;c anh em lại c&ugrave;ng nhau san sẻ kh&oacute; khăn.</p>\r\n<p><em>- Hơn 40&nbsp;năm trong nghề nhưng đến giờ Ph&uacute; Đ&ocirc;n vẫn chưa c&oacute; danh hiệu n&agrave;o. Anh c&oacute; bao giờ chạnh l&ograve;ng khi thấy anh em đồng nghiệp đều đ&atilde; l&agrave;&nbsp;&ldquo;&ocirc;ng nọ, b&agrave; kia&rdquo;?</em></p>\r\n<p>&nbsp;</p>\r\n<p>Chuyện danh hiệu c&ograve;n phải phụ thuộc v&agrave;o quy chế của nh&agrave; nước nữa. Đối với t&ocirc;i, sự ghi nhận lớn nhất ch&iacute;nh l&agrave; từ ph&iacute;a kh&aacute;n giả, đ&acirc;y l&agrave; động lực để t&ocirc;i cố gắng v&agrave; phấn đấu.</p>\r\n<p>C&ograve;n chuyện danh hiệu, b&ecirc;n trong rất nhiều vấn đề phức tạp v&agrave; kh&oacute; khăn.</p>\r\n<p><strong>Kh&ocirc;ng &aacute;p lực khi lấy vợ k&eacute;m 25 tuổi</strong></p>\r\n<p><em><strong>-&nbsp;</strong>Đ&atilde; gần lục tuần lại c&oacute; c&ocirc; vợ trẻ đẹp, k&eacute;m m&igrave;nh tới 25 tuổi, anh c&oacute; chịu nhiều &aacute;p lực về việc phải kiếm tiền để lo cho cuộc sống gia đ&igrave;nh, vợ con kh&ocirc;ng?</em></p>\r\n<p>Từ trước đến giờ, t&ocirc;i chưa từng c&oacute; &aacute;p lực về chuyện kiếm tiền, trang trải cuộc sống gia đ&igrave;nh. &Aacute;p lực duy nhất b&acirc;y giờ l&agrave; vấn đề sức khỏe. Khi đến một độ tuổi nhất định, sức khỏe kh&ocirc;ng c&ograve;n dồi d&agrave;o th&igrave; t&ocirc;i sẽ phải lựa chọn giữa việc n&agrave;y hay việc kia. M&igrave;nh phải suy t&iacute;nh nhiều lắm chứ kh&ocirc;ng thể &ocirc;m đồm tất cả mọi thứ như hồi c&ograve;n trẻ được. Điều đ&oacute; sẽ g&acirc;y kh&oacute; khăn cho anh em đồng nghiệp, đồng thời ảnh hưởng đến chất lượng những vở diễn m&agrave; m&igrave;nh tham gia.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-24/1464073855-1464069202-phu-don--2-.jpg" alt="Vo cua dien vien Phu Don - 3" /></p>\r\n<p>Ph&uacute; Đ&ocirc;n v&agrave; b&agrave; x&atilde; k&eacute;m 25 tuổi.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- &Iacute;t ra anh cũng phải đặt mục ti&ecirc;u kiểu:&nbsp;v&igrave; c&oacute; vợ trẻ, con nhỏ n&ecirc;n phải cố gắng để kiếm được một khoản v&agrave;i chục triệu chứ?</em></p>\r\n<p>T&ocirc;i l&agrave; một người c&oacute; cuộc sống tương đối giản dị, chưa bao giờ đặt &aacute;p lực cho bản th&acirc;n l&agrave; một th&aacute;ng phải kiếm được bao nhi&ecirc;u tiền. &nbsp;C&oacute; thể, th&aacute;ng n&agrave;y m&igrave;nh kiếm được hơn mức chi ti&ecirc;u th&igrave; sẽ bỏ ra tiết kiệm, c&ograve;n th&aacute;ng n&agrave;o thiếu th&igrave; lạ lấy ra để b&ugrave; v&agrave;o.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- C&oacute; người bảo anh l&agrave; người may mắn khi lấy được c&ocirc; vợ trẻ đẹp, k&eacute;m m&igrave;nh tận 2 gi&aacute;p?</em></p>\r\n<p>T&ocirc;i nghĩ may mắn l&agrave; một phần tất yếu trong cuộc sống nhưng đ&acirc;u thể may mắn m&atilde;i được. Cuộc sống n&agrave;y, một &nbsp;phần l&agrave; do duy&ecirc;n số nhưng ngo&agrave;i ra cũng l&agrave; những sự nỗ lực của bản th&acirc;n m&igrave;nh nữa đấy.</p>\r\n<p><em>- Anh c&oacute; phải cố gắng về sức khỏe v&agrave; ngoại h&igrave;nh để ph&ugrave; hợp, tương đồng với b&agrave; x&atilde; m&igrave;nh kh&ocirc;ng?</em></p>\r\n<p>Thực sự ng&agrave;y xưa thế n&agrave;o b&acirc;y giờ t&ocirc;i vẫn vậy.</p>\r\n<p><em>- Ở độ tuổi n&agrave;y v&agrave; với m&aacute;i ấm đang c&oacute;, anh nghĩ thế n&agrave;o về hạnh ph&uacute;c?</em></p>\r\n<p>Hạnh ph&uacute;c với t&ocirc;i l&agrave; một phạm tr&ugrave; qu&aacute; lớn nhưng b&acirc;y giờ t&ocirc;i c&oacute; thể khẳng định l&agrave; m&igrave;nh h&agrave;i l&ograve;ng với cuộc sống của đang c&oacute;.</p>\r\n<p>&nbsp;</p>\r\n<p><em>- Anh nghĩ sao về sự c&ocirc; đơn, trắc trở của người nghệ sĩ? Đ&oacute; c&oacute; phải l&agrave; sự đ&aacute;nh đổi ph&iacute;a sau &aacute;nh h&agrave;o quang của s&acirc;n khấu?</em></p>\r\n<p>&nbsp;</p>\r\n<p>C&aacute;i n&agrave;y t&ocirc;i lại tin v&agrave;o số phận. Mỗi người c&oacute; một ph&uacute;c, một phận ri&ecirc;ng, m&igrave;nh c&oacute; cố gắng khắc phục cũng chỉ được một phần.</p>\r\n<p>Cuộc sống n&agrave;y th&igrave; thi&ecirc;n định 50% &ndash; nh&acirc;n định 50%. Nếu như m&igrave;nh cố gắng th&igrave; c&oacute; thể đẩy phần &ldquo;nh&acirc;n định&rdquo; l&ecirc;n cao hơn con số 50, nhưng ngược lại, nếu kh&ocirc;ng cố gắng, th&igrave; con số &ldquo;thi&ecirc;n định&rdquo; c&oacute; thể l&agrave; 60, 70.</p>\r\n<p>Nhưng d&ugrave; con người c&oacute; cố gắng thế n&agrave;o, t&ocirc;i vẫn tin rằng c&oacute; một ch&uacute;t g&igrave; trong đời mỗi con người l&agrave; do số phận quyết định.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><em>Xin cảm ơn anh về cuộc tr&ograve; chuyện n&agrave;y!</em></p>', '1464073854-1464069202-phu-don--3-.jpg', '2016-05-24 16:24:41', '2016-05-24 16:24:41'),
(4, 'Nhan sắc cuốn hút của nữ chính hot nhất phim "Vượt ngục"', '<p>&nbsp;</p>\r\n<p>Sarah Wayne Callies l&agrave; nữ diễn vi&ecirc;n kh&ocirc;ng c&ograve;n xa &nbsp;lạ tại Hollywood với vai nữ ch&iacute;nh trong nhiều loạt phim truyền h&igrave;nh h&igrave;nh đ&aacute;m. Hai trong số những "bom tấn" c&ocirc; từng tham gia ch&iacute;nh l&agrave; <em>Vượt ngục</em> (Prison Break) v&agrave; <em>X&aacute;c sống</em> (The Walking Dead).</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976825-1463972830-sara-tancredi--1-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 1" /></p>\r\n<p>Nữ diễn vi&ecirc;n Sarah Wayne Callies.</p>\r\n<p>Sarah Wayne Callies&nbsp;sinh năm 1977 tại bang Ilinois nhưng cả gia đ&igrave;nh c&ocirc; đ&atilde; chuyển tới sinh sống tại đảo Hawaii từ năm c&ocirc;&nbsp;1 tuổi. Mặc d&ugrave; cả bố v&agrave; mẹ đều l&agrave; gi&aacute;o sư giảng dạy tại trường Đại học Hawaii nhưng Sarah Wayne lại kh&ocirc;ng c&oacute; hứng th&uacute; để tiếp bước nghề nghiệp của bố mẹ.</p>\r\n<p>C&ocirc; g&aacute;i c&oacute; vẻ bề ngo&agrave;i rất dễ tạo thiện cảm n&agrave;y quyết định con đường diễn xuất sau khi tốt nghiệp cấp 3 bằng c&aacute;ch đăng k&yacute; theo học trường Cao đẳng Dartmouth (Dartmouth College) v&agrave; Học viện nghệ thuật quốc gia Denver (Denver''s National Theater Conservatory). Năm 2002, Sarah tốt nghiệp với tấm bằng thạc sĩ.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976825-1463972830-sara-tancredi--3-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 2" /></p>\r\n<p align="center">Vai diễn b&aacute;c sĩ Sara Tancredi đ&atilde; đưa t&ecirc;n tuổi&nbsp;Sarah Wayne Callies th&agrave;nh một ng&ocirc;i sao lớn.</p>\r\n<p>Năm 2003,&nbsp;Sarah Wayne Callies chuyển đến New York để t&igrave;m kiếm cơ hội tr&ecirc;n con đường trở th&agrave;nh diễn vi&ecirc;n chuy&ecirc;n nghiệp. C&oacute; nhan sắc xinh đẹp n&ecirc;n con đường đến với sự nổi tiếng của&nbsp;Sarah Wayne Callies nhanh hơn nhiều so với Wentworth Miller - người đ&oacute;ng cặp c&ugrave;ng c&ocirc; trong <em>Vượt ngục.</em></p>\r\n<p>Hai năm sau khi đến New York v&agrave; chỉ sau v&agrave;i vai kh&aacute;ch mời, Sarah được nhận hợp đồng với h&atilde;ng FOX để tham gia vai b&aacute;c sĩ Sara Tancredi trong loạt phim <em>Vượt ngục</em>.</p>\r\n<p>Trong <em>Vượt ngục</em>, Sarah v&agrave;o&nbsp;vai một b&aacute;c sĩ tận t&acirc;m nhưng lại phải l&ograve;ng&nbsp;g&atilde; t&ugrave; nh&acirc;n th&ocirc;ng minh Michaels Scofield. Để rồi từ đ&oacute;, c&ocirc; bị cuốn v&agrave;o v&ograve;ng xo&aacute;y của những cuộc trốn chạy kh&ocirc;ng hồi kết, vừa đấu tranh với bản th&acirc;n, vừa đấu tranh với những cuộc thanh trừng của tổ chức.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976825-1463973039-sara-tancredi--4-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 3" /></p>\r\n<p>Sarah Wayne Callies c&oacute; gương mặt v&agrave; thần th&aacute;i rất cuốn h&uacute;t.</p>\r\n<p>Xuất hiện trong cả&nbsp;5 phần của bom tấn truyền h&igrave;nh n&agrave;y, Sarah nhận&nbsp;được sự y&ecirc;u mến của h&agrave;ng triệu người tr&ecirc;n thế giới.</p>\r\n<p>Sau th&agrave;nh c&ocirc;ng của <em>Vượt ngục</em>, Sarah tiếp tục nhận vai Lori Grimmes trong bộ phim truyền h&igrave;nh đ&aacute;m của h&atilde;ng AMC: <em>X&aacute;c sống</em> (The Walking Dead). Tuy chỉ xuất hiện trong 2 phần nhưng Sarah tiếp tục nhận được những phản hồi t&iacute;ch cực v&agrave;&nbsp;sự y&ecirc;u th&iacute;ch từ kh&aacute;n giả khắp thế giới nhờ ngoại h&igrave;nh xinh đẹp, diễn xuất ấn tượng.</p>\r\n<p>Sau một thời im ắng, Sarah ch&iacute;nh thức th&ocirc;ng b&aacute;o về sự trở lại của m&igrave;nh trong phần mới của <em>Vượt ngục</em> 2016. C&ocirc; sẽ c&ugrave;ng những "đồng đội" trong chuyến đ&agrave;o tho&aacute;t khi xưa trở lại để đi t&igrave;m Michaels Scofield vốn đang bị giam trong một nh&agrave; t&ugrave; b&iacute; mật.</p>\r\n<p><em>Một số h&igrave;nh ảnh của&nbsp;Sarah Wayne Callies:</em></p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976829-1463972830-sara-tancredi--7-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 4" /></p>\r\n<p>Sarah trong tạo h&igrave;nh của nh&acirc;n vật b&aacute;c sĩ Sara Tancredi.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976829-1463972830-sara-tancredi--9-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 5" /></p>\r\n<p>Nữ diễn vi&ecirc;n g&acirc;y ấn tượng nhờ ngoại h&igrave;nh bụi bặm nhưng vẫn c&oacute; n&eacute;t nữ t&iacute;nh.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976829-1463972830-sara-tancredi--10-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 6" /></p>\r\n<p>Sarah c&oacute; chiều cao 1m75 v&agrave; số đo 3 v&ograve;ng như một người mẫu.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976829-1463972830-sara-tancredi--11-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 7" /></p>\r\n<p>C&ocirc; c&oacute; phong c&aacute;ch bụi bặm, c&aacute; t&iacute;nh.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976829-1463972830-sara-tancredi--6-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 8" /></p>\r\n<p>Nhưng cũng vẫn rất dịu d&agrave;ng khi cần thiết.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976830-1463972830-sara-tancredi--12-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 9" /></p>\r\n<p>Sara v&agrave;o vai Lori Grimmes trong "X&aacute;c sống".</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976830-1463972830-sara-tancredi--15-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 10" /></p>\r\n<p align="center">Bộ 3 nổi tiếng trong "Vượt ngục".</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463976830-1463972830-sara-tancredi--14-.jpg" alt="Nhan sắc cuốn h&uacute;t của nữ ch&iacute;nh hot nhất phim &amp;#34;Vượt ngục&amp;#34; - 11" /></p>\r\n<p>Wenworth Miller, Sarah Callies v&agrave; nam diễn vi&ecirc;n Robert Knepper.</p>', '1463976825-1463972830-sara-tancredi--1-.jpg', '2016-05-24 16:24:34', '2016-05-24 16:24:34'),
(5, 'Lâm Tâm Như khen Hoắc Kiến Hoa đẹp trai', '<p>H&ocirc;m 21.2, L&acirc;m T&acirc;m Như bị c&aacute;c ph&oacute;ng vi&ecirc;n trang <em>Sohu</em> ph&aacute;t hiện c&oacute; mặt tại s&acirc;n bay Thanh Đảo (Trung Quốc) để trở về Đ&agrave;i Bắc (Đ&agrave;i Loan) sau chuyến thăm bạn trai tại đ&acirc;y, t&agrave;i tử Hoắc Kiến Hoa.</p>\r\n<p>Đ&acirc;y l&agrave; lần đầu ti&ecirc;n nữ diễn vi&ecirc;n xuất hiện trước c&ocirc;ng ch&uacute;ng kể từ sau khi cặp đ&ocirc;i c&ocirc;ng khai chuyện t&igrave;nh cảm.</p>\r\n<p><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-23/1463973429-1463933009-lam-tam-nhu-1.jpg" alt="L&acirc;m T&acirc;m Như khen Hoắc Kiến Hoa đẹp trai - 1" /></p>\r\n<p>L&acirc;m T&acirc;m Như tại s&acirc;n b&acirc;y Thanh Đảo sau khi thăm bạn trai trở về.</p>\r\n<p>Ng&ocirc;i sao <em>Ho&agrave;n Ch&acirc;u C&aacute;ch C&aacute;ch</em> rạng rỡ trong trang phục &aacute;o T-shirt trẻ trung kết hợp với quần jeans, c&ocirc; đội mũ bere v&agrave; đeo k&iacute;nh m&aacute;t nhưng c&aacute;c ph&oacute;ng vi&ecirc;n săn tin ở đại lục vẫn nhận ra.</p>\r\n<p>&nbsp;</p>\r\n<p>Tại s&acirc;n bay Thanh Đảo, L&acirc;m T&acirc;m Như đ&atilde; vui vẻ trả lời truyền th&ocirc;ng Trung Quốc về chuyện t&igrave;nh cảm giữa c&ocirc; với t&agrave;i tử điển trai họ Hoắc: &ldquo;<em>Anh ấy đẹp trai lắm v&agrave; cũng l&agrave; một người đ&agrave;n &ocirc;ng tốt</em>&rdquo;, nữ diễn vi&ecirc;n 40 tuổi cười rạng rỡ chia sẻ.</p>', '1463973429-1463933009-lam-tam-nhu-1.jpg', '2016-05-24 16:24:00', '2016-05-24 16:24:00'),
(6, 'Vẻ ngoài 30 năm chưa hề thay đổi của tài tử Tom Cruise', '<p>Mới đ&acirc;y, c&aacute;c fan đ&atilde; lục lại ảnh của Tom Cruise nh&acirc;n kỉ niệm bộ phim <em>Top Gun</em> của nam diễn vi&ecirc;n n&agrave;y ra mắt tr&ograve;n 30. Kh&aacute;n giả đ&atilde; v&ocirc; c&ugrave;ng ngạc nhi&ecirc;n khi nhận ra, Tom Cruise vẫn giữ được vẻ ngo&agrave;i trẻ trung phong độ của m&igrave;nh sau ba thập kỷ, trong khi đ&oacute; người bạn diễn của anh - nữ diễn vi&ecirc;n Kelly McGillis đ&atilde; xuống sắc.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915138-1463910799-tom-cruise--1-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 1" /></p>\r\n<p>Tom Cruise trong bộ phim <em>Top Gun&nbsp;</em>ra mắt năm 1986. Ch&iacute;nh bộ phim n&agrave;y cũng gi&uacute;p nam diễn vi&ecirc;n trở th&agrave;nh mẫu đ&agrave;n &ocirc;ng l&yacute; tưởng trong l&ograve;ng c&aacute;c fan nữ.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915138-1463910799-tom-cruise--2-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 2" /></p>\r\n<p>Tom Cruise trong <em>Nhiệm vụ bất khả thi 5</em> ra mắt v&agrave;o năm 2015. Ở tuổi 53, Tom Cruise vẫn tự m&igrave;nh đ&oacute;ng c&aacute;c cảnh h&agrave;nh động mạo hiểm như đu m&igrave;nh tr&ecirc;n m&aacute;y bay.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915138-1463911167-tom-cruise--7-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 3" /></p>\r\n<p align="center">Trải qua 30 năm, gương mặt Tom Cruise gần như kh&ocirc;ng c&oacute; g&igrave; thay đổi.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915138-1463911167-tom-cruise--9-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 4" /></p>\r\n<p align="center">Nam diễn vi&ecirc;n vẫn lọt v&agrave;o top danh s&aacute;ch những gương mặt diễn vi&ecirc;n nam&nbsp;hấp dẫn nhất m&agrave;n bạc.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915138-1463911167-tom-cruise--10-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 5" /></p>\r\n<p>Tuy nhi&ecirc;n, nhiều &yacute; kiến cho rằng Tom Cruise đ&atilde; sử dụng c&aacute;c biện ph&aacute;p dao k&eacute;o để giữ g&igrave;n tu&ocirc;i thanh xu&acirc;n&nbsp;của m&igrave;nh.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915139-1463910799-tom-cruise--3-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 6" /></p>\r\n<p align="center">Đ&oacute;ng cặp c&ugrave;ng Tom Cruise trong <em>Top Gun</em>&nbsp;l&agrave; nữ diễn vi&ecirc;n Kelly McGillis.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915139-1463910799-tom-cruise--4-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 7" /></p>\r\n<p align="center">Khi v&agrave;o vai Charlie trong phim, nữ diễn vi&ecirc;n tr&ocirc;ng trẻ trung hơn nhiều so với tuổi 29.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915139-1463910799-tom-cruise--5-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 8" /></p>\r\n<p>Tuy nhi&ecirc;n, 30 năm sau, Kelly McGillis đ&atilde; bước sang tuổi 57 với ngoại h&igrave;nh của một người trung ni&ecirc;n.</p>\r\n<p align="center"><img class="news-image" src="http://24h-img.24hstatic.com/upload/2-2016/images/2016-05-22/1463915139-1463911167-tom-cruise--8-.jpg" alt="Vẻ ngo&agrave;i 30 năm chưa hề thay đổi của t&agrave;i tử Tom Cruise - 9" /></p>\r\n<p align="center">H&igrave;nh ảnh đẹp của cặp đ&ocirc;i trong phim "Top Gun" ng&agrave;y n&agrave;o.</p>', '1463915138-1463910799-tom-cruise--1-.jpg', '2016-05-24 16:24:55', '2016-05-24 16:24:55'),
(7, 'Lịch sử tình trường toàn trai đẹp của Gigi Hadid', '<p class="the-article-summary">Sinh ra trong gia đ&igrave;nh gi&agrave;u c&oacute;, lại thừa hưởng n&eacute;t đẹp sắc sảo từ mẹ, kh&ocirc;ng kh&oacute; hiểu khi trước Zayn Malik, Gigi Hadid từng c&oacute; mối quan hệ t&igrave;nh cảm với rất nhiều &ldquo;trai đẹp" kh&aacute;c.</p>\r\n<div class="the-article-body">\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Patrick.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 1" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Patrick Uretz l&agrave; một trong những bạn trai &iacute;t nổi tiếng nhất của Gigi Hadid. Cả 2 quen nhau v&agrave;o năm 2011, khi Patrick 24 tuổi, trong khi Gigi chỉ vừa bước qua tuổi 17.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Cody.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 2" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Một trong những mối t&igrave;nh dai dẳng đ&aacute;ng nhớ kh&aacute;c của Gigi Hadid l&agrave; anh ch&agrave;ng ca sĩ điển trai Cody Simpson. Cả 2 đ&atilde; c&ugrave;ng nhau chụp h&igrave;nh, chia tay, trở lại với nhau, chia sẻ những kỷ niệm ngọt ng&agrave;o, rồi lại chia tay.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Daniel.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 3" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Sau khi bị bắt gặp hẹn h&ograve; c&ugrave;ng nhau hồi th&aacute;ng 10/2014, nhiều lời đồn đo&aacute;n cũng dấy l&ecirc;n về mối quan hệ giữa Gigi v&agrave; ng&ocirc;i sao<em> Teen Wolf</em> Daniel Sharman. Tuy nhi&ecirc;n, cuối c&ugrave;ng, c&acirc;u chuyện t&igrave;nh cảm của cả 2 cũng chỉ dừng lại ở đ&oacute; m&agrave; kh&ocirc;ng c&oacute; bất cứ dấu hiệu n&agrave;o đi xa hơn.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Lewis.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 4" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Lewis Hamilton cũng từng tốn kh&ocirc;ng &iacute;t giấy mực của b&aacute;o giới bởi mối t&igrave;nh với Gigi Hadid, khi cả 2 c&oacute; sự ch&ecirc;nh lệch tuổi t&aacute;c qu&aacute; lớn. V&agrave;o thời điểm x&aacute;c nhận hẹn h&ograve;, Lewis 30 tuổi, c&ograve;n Gigi chỉ mới 19 tuổi.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/ExZZAhkNWTMMFRpIKxMBHQdZAwQT.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 5" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Nick Jonas l&agrave; c&aacute;i t&ecirc;n kế tiếp xuất hiện trong danh s&aacute;ch bạn trai của Gigi. Sau khi cả 2 chia tay một thời gian ngắn sau đ&oacute;, Nick Jonas đ&atilde; quen th&ecirc;m một b&oacute;ng hồng kh&aacute;c l&agrave; nữ diễn vi&ecirc;n Lily Collins.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Joe1.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 6" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Sau Nick Jonas, th&aacute;ng 10/2014, Gigi Hadid tiếp tục c&oacute; mối quan hệ th&acirc;n mật với người anh trong nh&oacute;m l&agrave; Joe Jonas sau qu&atilde;ng thời gian d&agrave;i l&agrave;m bạn từ khi Gigi mới 13 tuổi.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/Joe2.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 7" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Chia sẻ về chuyện t&igrave;nh cảm giữa bạn g&aacute;i cũ v&agrave; em trai, Nick Jonas cho biết: &ldquo;T&ocirc;i th&iacute;ch việc họ ở b&ecirc;n nhau. Joe v&agrave; t&ocirc;i rất th&acirc;n nhau. Ch&uacute;ng t&ocirc;i lu&ocirc;n hỗ trợ lẫn nhau, v&igrave; thế sắp tới t&ocirc;i sẽ c&ograve;n gặp lại c&ocirc; ấy (Gigi) nhiều lần nữa. T&ocirc;i thật sự cảm thấy rất vui cho họ. T&ocirc;i nghĩ họ đang hạnh ph&uacute;c b&ecirc;n nhau.&rdquo;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/02ZaynMalikandGigiHadidCostumeInstituteGalaMetgala2016billboard6501548_1.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 8" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Sau khi chia tay Joe Jonas kh&ocirc;ng bao l&acirc;u, Gigi Hadid tiếp tục g&acirc;y nhiều tranh c&atilde;i khi ch&oacute;ng v&aacute;nh h&ograve; hẹn c&ugrave;ng Zayn Malik. Một cư d&acirc;n mạng tr&ecirc;n Twitter mỉa mai c&ocirc; rằng: "H&atilde;y tập c&aacute;ch catwalk tốt thay v&igrave; cứ 2 tuần lại hẹn h&ograve; bạn trai mới". Đ&aacute;p lại, c&ocirc; khẳng định c&ocirc; chỉ hẹn h&ograve; với 3 người đ&agrave;n &ocirc;ng trong v&ograve;ng 3 năm qua.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/rs1024x7591606021654121024taylorgigicalvinzayn1464919019558.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 9" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Mới đ&acirc;y nhất, th&ocirc;ng tin Gigi Hadid lại tiếp tục chia tay với Zayn khiến nhiều người h&acirc;m mộ kh&ocirc;ng khỏi sửng sốt, v&igrave; chỉ 2 ng&agrave;y trước, nữ ca sĩ Taylor Swift cũng vừa chia tay bạn trai Calvin Harris sau 15 th&aacute;ng hẹn h&ograve;.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/rs_634x1024150510155655634taylorswifthikinggigihadidls51015.jpg" alt="Lich su tinh truong toan trai dep cua Gigi Hadid hinh anh 10" /><a class="btnSlideshow" href="http://news.zing.vn/lich-su-tinh-truong-toan-trai-dep-cua-gigi-hadid-post654696.html#slideshow">Ph&oacute;ng to</a></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Theo <em>E! News</em>, hiện giờ Gigi v&agrave; c&ocirc; bạn th&acirc;n Taylor Swift đang t&igrave;m đến nhau như chỗ dựa v&agrave;o nhau sau 2 cuộc chia tay đ&igrave;nh đ&aacute;m. B&ecirc;n cạnh việc đứng sau hỗ trợ v&agrave; động vi&ecirc;n lẫn nhau, Gigi cho biết c&ocirc; hy vọng t&igrave;nh bạn với Taylor v&agrave; c&aacute;c sao nữ kh&aacute;c c&oacute; thể truyền cảm hứng cho nữ giới. &ldquo;Ch&uacute;ng t&ocirc;i muốn được biết đến như l&agrave; một nh&oacute;m c&aacute;c c&ocirc; g&aacute;i tử tế, hơn l&agrave; chỉ biết nh&otilde;ng nhẽo v&agrave; dựa dẫm v&agrave;o nhau.&rdquo;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', 'Patrick.jpg', '2016-06-03 12:03:30', '2016-06-03 12:03:30'),
(8, 'Biệt thự giá 2,65 triệu USD của vợ chồng Angelina Jolie', '<p class="the-article-summary">Những h&igrave;nh ảnh to&agrave;n cảnh biệt thự b&ecirc;n bờ biển mới mua của cặp đ&ocirc;i nổi tiếng được đăng tải tr&ecirc;n trang Sina.</p>\r\n<div class="the-article-body">\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933742_368011.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 1" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Đ&ocirc;i vợ chồng quyền lực Hollywood mới đ&acirc;y bỏ ra đến 2,65 triệu USD để tậu biệt thự cao cấp b&ecirc;n h&ograve;n đảo Majorca (T&acirc;y Ban Nha). Theo <em>Dailymail</em>, Angelina Jolie v&agrave; Brad Pitt đưa ra quyết định sau khi đến mảnh đất xinh đẹp n&agrave;y du lịch.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933728_308791.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 2" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Biệt thự c&oacute; diện t&iacute;ch khoảng 900m2, gồm 8 ph&ograve;ng ngủ v&agrave; nằm ngay s&aacute;t biển thuộc thị trấn Andratx. Điểm ấn tượng của biệt thự l&agrave; khung cảnh n&ecirc;n thơ v&agrave; bể bơi ngay b&ecirc;n ngo&agrave;i hướng ra biển Địa Trung Hải. "Ngay khi đến biệt thự n&agrave;y, Brad Pitt đ&atilde; bị m&ecirc; hoặc. Anh ấy đ&atilde; c&oacute; &yacute; tưởng mua nh&agrave; ở Majorca v&agrave; phải mất một thời gian anh ấy mới t&igrave;m được biệt thự như &yacute;", một nh&agrave; kinh doanh bất động sản t&ecirc;n Alessandro Proto n&oacute;i.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img title="Biệt thự gi&aacute; 2,65 triệu USD của vợ chồng Angelina Jolie h&igrave;nh ảnh 3" src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933729_800068.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 3" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Theo m&ocirc; tả, biệt thự n&agrave;y kh&ocirc;ng rộng so với những khối bất động sản do cặp vợ chồng nổi tiếng sở hữu. Tuy nhi&ecirc;n, kh&ocirc;ng gian y&ecirc;n tĩnh. Ph&ograve;ng kh&aacute;ch tho&aacute;ng đ&uacute;ng chuẩn kiến tr&uacute;c cổ T&acirc;y Ban Nha.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933730_707261.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 4" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">Mọi g&oacute;c nh&igrave;n trong ph&ograve;ng kh&aacute;ch đều c&oacute; tầm nh&igrave;n hướng ra biển Địa Trung Hải.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933731_763679.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 5" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">Kh&ocirc;ng gian ph&ograve;ng bếp trang nh&atilde; với t&ocirc;ng m&agrave;u cổ, s&agrave;n đ&aacute;.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933733_215222.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 6" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Ph&ograve;ng ngủ với phong c&aacute;ch ho&agrave;ng gia.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933734_411153.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 7" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">Mỗi ph&ograve;ng ngủ c&oacute; một thiết kế nội thất kh&aacute;c biệt, tạo cảm gi&aacute;c mới lạ.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933735_149996.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 8" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">Những ph&ograve;ng l&agrave;m việc được thiết kế đơn giản, tạo đủ sự y&ecirc;n tĩnh để gia chủ tập trung l&agrave;m việc hoặc nghi&ecirc;n cứu kịch bản.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933736_470378.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 9" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">Kh&ocirc;ng gian ph&ograve;ng tắm đơn giản với tường sơn v&agrave;ng, ốp đ&aacute; v&acirc;n truyền thống.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table class="picture" align="center">\r\n<tbody>\r\n<tr>\r\n<td class="pic"><img src="http://img.v3.news.zdn.vn/w660/Uploaded/wopthuo/2016_06_03/704_1933740_452720.jpg" alt="Biet thu gia 2,65 trieu USD cua vo chong Angelina Jolie hinh anh 10" /></td>\r\n</tr>\r\n<tr>\r\n<td class="pCaption caption">\r\n<p>Angelina Jolie v&agrave; Brad Pitt gần đ&acirc;y li&ecirc;n tục vướng tin đồn rạn nứt t&igrave;nh cảm v&agrave; đổ vỡ h&ocirc;n nh&acirc;n. Nhưng cặp sao vẫn giữ im lặng. Với động th&aacute;i mua biệt thự mới, họ chứng tỏ tin đồn l&agrave; v&ocirc; căn cứ.</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>', '704_1933742_368011.jpg', '2016-06-03 12:03:27', '2016-06-03 12:03:27');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `type` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `pages`
--

TRUNCATE TABLE `pages`;
--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `body`, `status`, `type`) VALUES
(1, 'Giới thiệu', '<p>Giới thiệu</p>', 1, 'about');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE `schedule` (
  `id` int(11) UNSIGNED NOT NULL,
  `film_id` int(11) UNSIGNED NOT NULL,
  `cinema_id` int(11) UNSIGNED NOT NULL,
  `time` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `schedule`
--

TRUNCATE TABLE `schedule`;
--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `film_id`, `cinema_id`, `time`, `status`) VALUES
(1, 1, 1, '2016-04-06 20:55:00', 1),
(2, 1, 2, '2016-04-19 09:55:58', 0),
(3, 2, 3, '2016-04-16 20:00:29', 1),
(4, 4, 4, '2016-04-20 18:55:23', 1),
(5, 9, 2, '2016-04-23 11:55:15', 1),
(6, 9, 2, '2016-04-21 20:00:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) UNSIGNED NOT NULL,
  `schedule_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `film_type` int(11) UNSIGNED NOT NULL,
  `ticket_qty` tinyint(2) UNSIGNED NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `ticket`
--

TRUNCATE TABLE `ticket`;
--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`id`, `schedule_id`, `user_id`, `film_type`, `ticket_qty`, `status`) VALUES
(1, 1, 2, 1, 1, 1),
(2, 1, 4, 1, 1, 1),
(3, 1, 22, 1, 3, 0),
(4, 5, 1, 1, 1, 0),
(5, 6, 1, 1, 10, 0),
(6, 3, 1, 1, 3, 0),
(7, 4, 1, 2, 1, 0),
(8, 1, 1, 2, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_meta`
--

DROP TABLE IF EXISTS `ticket_meta`;
CREATE TABLE `ticket_meta` (
  `id` int(11) UNSIGNED NOT NULL,
  `ticket_id` int(11) UNSIGNED NOT NULL,
  `film_type` int(11) UNSIGNED NOT NULL,
  `chair` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `ticket_meta`
--

TRUNCATE TABLE `ticket_meta`;
--
-- Dumping data for table `ticket_meta`
--

INSERT INTO `ticket_meta` (`id`, `ticket_id`, `film_type`, `chair`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2),
(3, 2, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `ticket_price`
--

DROP TABLE IF EXISTS `ticket_price`;
CREATE TABLE `ticket_price` (
  `id` int(11) UNSIGNED NOT NULL,
  `cinema_id` int(11) UNSIGNED NOT NULL,
  `film_type` int(11) UNSIGNED NOT NULL,
  `price` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncate table before insert `ticket_price`
--

TRUNCATE TABLE `ticket_price`;
--
-- Dumping data for table `ticket_price`
--

INSERT INTO `ticket_price` (`id`, `cinema_id`, `film_type`, `price`) VALUES
(1, 1, 1, 80000),
(2, 1, 2, 120000),
(3, 2, 1, 90000),
(4, 2, 2, 140000),
(5, 3, 1, 100000),
(6, 3, 2, 120000),
(7, 4, 1, 110000),
(8, 4, 2, 140000),
(9, 5, 1, 85000),
(10, 5, 2, 125000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `role` smallint(2) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `user`
--

TRUNCATE TABLE `user`;
--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `fullname`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `mobile`, `address`, `status`, `role`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '_PdYmj4ultLhKmDzVUhYB2upaK9s-UGS', '$2y$13$.0utoYz.EPz0vEf8kPTSVuqd4jSfwjvFs/0tUjo19xcuvSnIAZCW2', '', 'admin@gmail.com', '0987654321', 'Việt Nam', 10, 20, 1459958604, 1464852842),
(2, 'nva', 'Nguyễn Văn A', '_PdYmj4ultLhKmDzVUhYB2upaK9s-UGS', '$2y$13$PAXCWKoh2gFOBFSEmm396eEkLJ9o/8eg5rXzL0mC44LYsrWcF6K7m', NULL, '', '0987654321', NULL, 0, 10, 1460368294, 1460368294),
(4, 'nvd', 'Nguyễn Văn D', 'e4VrxdeqfgiLDhtcppGRN9LbiXSEJrtW', '$2y$13$Xp/lys63W64LkqbIadkqrOdmf6ebao3VqK4op43FhgLroEN33vsu2', NULL, 'nvd@gmail.com', '0987654321', NULL, 10, 10, 1460606336, 1460606336),
(9, 'nve', 'Nguyễn Văn E', '9ffOITLD4trYnTNVKh9KfnUAqIAWdyk5', '$2y$13$FJ4fLogYXi0Ijr3jZ0.GOubXcStZiMo..WNolbRmvRp58EnBBvASm', NULL, 'nve@gmail.com', '0987654321', NULL, 10, 10, 1460607484, 1460607484),
(10, 'nvf', 'Nguyễn Văn F', 'kTZIfdLb-nqTtSOLqgG8qNP2Rym5_XVI', '$2y$13$VNwkdi19USRIXjDHAVJp4.jWdTQbU/X/s.0Ta9t9W8mqIhUjwqwcO', NULL, 'nvf@gmail.com', '0987654321', NULL, 10, 20, 1460608074, 1463238333),
(11, 'nvg', 'Nguyễn Văn G', 'okQS-v9IW1dmCWn1MaiHxN6JSqm2wH30', '$2y$13$RdUNN0rlMAUXpeLhr9w1R.puWRIx0TOB2kgGcp47qqnOzWRsnc9iu', NULL, 'nvg@gmail.com', '0987654321', NULL, 10, 10, 1460608120, 1460608120),
(12, 'aaa', 'AAA', '2cT06l9xgZ2i43U52D_9YI94vylK2MEt', '$2y$13$RZRQ7PgAJ.9oyULupMk9PulMvLMotw//z9Hn8MzYjND4lZhkgJPRK', NULL, 'aaa@gmail.com', '0987654321', NULL, 10, 10, 1460608195, 1460608195),
(13, 'nvh', 'Nguyễn Văn H', 'lSzVvBh_S0FdsVzyNGf9wJ5533O2xTXG', '$2y$13$vqV2SiwNr80I5bOIO/vmQOtaZ3mYHoG/ur6sziJYDDXovpctVOOlC', NULL, 'nvh@gmail.com', '0987654321', NULL, 10, 10, 1460609902, 1460609902),
(14, 'nvi', 'Nguyễn Văn I', 'fpo6zC4VOomhsdcRccuTNj-pWEQGGrMI', '$2y$13$yPqKS0Z5n1kbMGR0oolge.qYXe/2u7zxqieH.HqMYhqdocs6FGLNi', NULL, 'nvi@gmail.com', '0987654321', NULL, 10, 10, 1460610006, 1460610006),
(15, 'nvj', 'Nguyễn Văn J', 'Ym_IBDRZyrmhXS-oKo82pE0OD8KaxsTi', '$2y$13$wCSE6QzSttKNCIDg1Rpl5ez2zB6/oknRgcGzcU7ifBoPMYvpU1qXO', NULL, 'nvj@gmail.com', '0987654321', NULL, 10, 10, 1460610108, 1460610108),
(17, 'nvz', 'Nguyễn Văn Z', 'YT0HXdhuvbuUjr7aPnbwWTrd0WAI3g8C', '$2y$13$GKE/4IRuYnk1lNJbdAvinOSpyyHJwxY2hOPKmdq3Y7V7qtEY7S6i2', NULL, 'nvz@gmail.com', '0987654321', NULL, 10, 10, 1463234377, 1463234377),
(18, 'nvy', 'Nguyễn Văn Y', '_ulczkxeKoD4KLMcLF9YsFGe0ChPI1uB', '$2y$13$Gv0aprZgSDovWi.pIFyg5uptmBu.sKFLGjIekOLeEoIGZtUBMNNf2', NULL, 'nvy@gmail.com', '0987654321', NULL, 10, 10, 1463234450, 1463234450),
(20, 'admin123', 'Admin 123', 'W0VF6JXYeA_VE7SAW1DCu0Otna9FHe8E', '$2y$13$NFJGRUnRiPeLKyrie0bQpekLkzuPp5itqPRlKWspXvHuangbanb/e', NULL, 'admin123@gmail.com', '0987654321', NULL, 10, 10, 1463234897, 1463234897),
(21, 'nvo', 'Nguyễn Văn O', 'Uud5oBwzdzeRNM_fhI0M3T-qurIB60-0', '$2y$13$M4kqyD5dVSry0nFmEHmG1.S1zQA./n1aojEsS34.Oy.3T7sZ7IeSm', NULL, 'nvo@gmail.com', '0987654321', NULL, 10, 10, 1463498545, 1463498545),
(22, 'adadad', 'adadad', 'xQAhM__0msG6y0SPCFaxmCsUWYQEumnV', '$2y$13$.LcHR/rAg/wvnajtOh.ou.T.ccghwhXkEB/AFYitQNzMNgLznWsIe', NULL, '2423ada@gmail.com', '424251251', 'adada', 10, 10, 1464919972, 1464919972),
(23, '1212', '1212', 'Iz8kArNxpDgJleFA-KCVvooLLraIm5aM', '$2y$13$bhRUYUcYwHfKvbRmxMmZkuW2uktdF5w019rkUmGSQILeHtxBDKUB.', NULL, '1212@gmail.com', '123457890', NULL, 10, 10, 1464945241, 1464945241),
(24, '1341241', '1341241', '0ifrFw0BJUwakAZw2hkbXSmmMcjILRG3', '$2y$13$IvDye.XMT4gtAcUzxkotHOZz7WEOXCnBonsB8iM3F1RDl37wAhHZi', NULL, '1341241@gmail.com', '1341241', NULL, 10, 10, 1464945264, 1464945264);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cinema`
--
ALTER TABLE `cinema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `film`
--
ALTER TABLE `film`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `cate_id` (`cate_id`);

--
-- Indexes for table `film_cate`
--
ALTER TABLE `film_cate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `film_type`
--
ALTER TABLE `film_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `film_id` (`film_id`),
  ADD KEY `cinema_id` (`cinema_id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedule_id` (`schedule_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `film_type` (`film_type`);

--
-- Indexes for table `ticket_meta`
--
ALTER TABLE `ticket_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ticket_id` (`ticket_id`),
  ADD KEY `film_type` (`film_type`);

--
-- Indexes for table `ticket_price`
--
ALTER TABLE `ticket_price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cinema_id` (`cinema_id`),
  ADD KEY `film_type` (`film_type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cinema`
--
ALTER TABLE `cinema`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `film`
--
ALTER TABLE `film`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `film_cate`
--
ALTER TABLE `film_cate`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `film_type`
--
ALTER TABLE `film_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ticket_meta`
--
ALTER TABLE `ticket_meta`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ticket_price`
--
ALTER TABLE `ticket_price`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
