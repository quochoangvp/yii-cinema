<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Film;
use common\models\Schedule;
use common\models\Cinema;
use common\models\Ticket;
use common\models\TicketPrice;
use common\models\Pages;
use yii\data\Pagination;
use common\models\News;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $query = News::find()->select('id, title, thumbnail')->orderBy(['created_at' => SORT_DESC]);
        $news = $query->offset(0)->limit(10)->asArray()->all();

        $filmScreened = Film::getFilmScreened([], 4);
        return $this->render('index', [
            'filmScreened' => $filmScreened,
            'news' => $news
        ]);
    }

    public function actionLienHe()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $status = true;
            $message = '';
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                $model = new ContactForm();
                $message = 'Gửi liên hệ thành công, chúng tôi sẽ phải hồi thư của bạn trong thời gian sớm nhất';
            } else {
                $message = 'Lỗi hệ thống, không gửi được email, vui lòng thử lại';
                $status = false;
            }
            return $this->render('contact', [
                'model' => $model,
                'message' => $message,
                'status' => $status
            ]);
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionSearch()
    {
        $filmScreened = Film::getFilmScreened(['LIKE', 'film.name', Yii::$app->request->get('search-input')]);
        return $this->render('search', [
            'filmScreened' => $filmScreened
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $schedule_id = intval(Yii::$app->request->get('id'));
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    if ($schedule_id > 0) {
                        return $this->redirect('@web/site/dat-ve/?id=' . $schedule_id);
                    } else {
                        return $this->goHome();
                    }
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionLichChieu()
    {
        $currentFilm = new \stdClass;
        $currentFilm->id = 0;
        $currentCinema = new \stdClass;
        $currentCinema->id = 0;

        $films = Film::find()->all();
        $currentFilmId = intval(Yii::$app->request->get('film_id'));
        foreach ($films as $film) {
            if ($film->id == $currentFilmId) {
                $currentFilm = $film;
            }
        }

        $cinemas = Cinema::find()->all();
        $currentCinemaId = intval(Yii::$app->request->get('cinema_id'));
        foreach ($cinemas as $cinema) {
            if ($cinema->id == $currentCinemaId) {
                $currentCinema = $cinema;
            }
        }

        $schedules = Schedule::find()->where([
            'cinema_id' => $currentCinema->id,
            'film_id' => $currentFilm->id,
        ])->orderBy('time')->all();
        return $this->render('lichChieu', [
            'films' => $films,
            'cinemas' => $cinemas,
            'currentFilm' => $currentFilm,
            'currentCinema' => $currentCinema,
            'schedules' => $schedules
        ]);
    }

    public function actionPhimDangChieu()
    {
        $query = Film::find()->select('film.*, film_cate.name as cate_name')
            ->leftJoin('film_cate', '`film_cate`.`id` = `film`.`cate_id`')
            ->where(['film.status' => 1])
            ->with('filmCate')
            ->orderBy([
                'start' => SORT_DESC
            ]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 8]);
        $filmScreened = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('phimDangChieu', [
             'filmScreened' => $filmScreened,
             'pages' => $pages,
        ]);
    }

    public function actionChiTiet($slug = '')
    {
        $currentFilm = Film::getBySlug($slug);
        if ($currentFilm) {
            $films = Film::find()->all();
            foreach ($films as $film) {
                if ($film->id == $currentFilm->id) {
                    $currentFilm = $film;
                }
            }

            $schedules = Schedule::find()
                ->select('schedule.*, cinema.name as cinema_name')
                ->leftJoin('cinema', '`cinema`.`id` = `schedule`.`cinema_id`')
                ->where([
                    'schedule.status' => 1,
                    'schedule.film_id' => $currentFilm->id
                ])
                ->with('cinemas')
                ->asArray()
                ->orderBy([
                    'time' => SORT_DESC
                ])
                ->all();

            return $this->render('chiTiet', [
                'film' => $currentFilm,
                'schedules' => $schedules
            ]);
        } else {
            return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
        }
    }

    public function actionGioiThieu()
    {
        $content = Pages::find()->where(['type' => 'about', 'status' => 1])->one();
        if ($content) {
            $content = $content->body;
        } else {
            $content = '';
        }
        return $this->render('about', [
            'content' => $content
        ]);
    }

    public function actionRapGiaVe()
    {
        $cinemas = Cinema::find()->all();
        $cinema_id = intval(Yii::$app->request->get('id'));
        if (!$cinema_id) {
            $cinema_id = 1;
        }
        $cinemaActive = Cinema::find()->where(['id' => $cinema_id])->one();
        return $this->render('rapGiaVe', [
            'cinemas' => $cinemas,
            'cinemaActive' => $cinemaActive
        ]);
    }

    public function actionDatVe()
    {
        $schedule_id = intval(Yii::$app->request->get('id'));
        if (Yii::$app->user->isGuest) {
            return $this->redirect('@web/site/signup?id=' . $schedule_id);
        }
        $ticket = new Ticket();

        $schedule = Schedule::getScheduleDetails($schedule_id);
        $schedule['price'] = TicketPrice::find()->where(['cinema_id' => $schedule['cinema_id'], 'film_type' => $schedule['film_type_id']])->one()->price;
        $schedule['date'] = $this->_datetime_vi($schedule['time']);
        $schedule['time'] = date_format(date_create($schedule['time']), 'H:i');

        if ($ticket->load(Yii::$app->request->post())) {
            $ticket->status = 0;
            $ticket->user_id = Yii::$app->user->identity->id;
            $ticket->schedule_id = Yii::$app->request->post('Ticket')['schedule_id'];
            $ticket->film_type = Yii::$app->request->post('Ticket')['film_type'];
            $ticket->schedule_id = Yii::$app->request->post('Ticket')['schedule_id'];
            if ($ticket->save()) {
                return $this->redirect('@web/site/dat-ve-thanh-cong');
            } else {
                return $this->render('datVe', [
                    'schedule' => $schedule,
                    'message' => 'Không thể đặt vé do lỗi hệ thống, xin vui lòng thử lại'
                ]);
            }
        } else {
            return $this->render('datVe', [
                'schedule' => $schedule,
                'message' => 'Không thể đặt vé do lỗi hệ thống, xin vui lòng thử lại'
            ]);
        }

        return $this->render('datVe', [
            'schedule' => $schedule
        ]);
    }

    private function _datetime_vi($date_time = false) {
        $time = ($date_time) ? $date_time : date('Y-m-d H:j:s');
        $time = date_create($time);
        $date = date_format($time, 'd/m/Y');
        $day = 'Chủ nhật';
        switch (date("N", $time->getTimestamp())) {
            case '1':
                $day = 'Thứ hai';
                break;
            case '2':
                $day = 'Thứ ba';
                break;
            case '3':
                $day = 'Thứ tư';
                break;
            case '4':
                $day = 'Thứ năm';
                break;
            case '5':
                $day = 'Thứ sáu';
                break;
            case '6':
                $day = 'Thứ bảy';
                break;
            default:
                break;

        }
        return $day . ' ' . $date;
    }

    public function actionDatVeThanhCong()
    {
        return $this->render('dat-ve-thanh-cong');
    }

    public function actionGetCinemaByFilm()
    {
        $film_id = intval(Yii::$app->request->post('film_id'));
        $cinema = Cinema::getCinemaByFilmId($film_id);
        $cinemaOption = '';
        foreach ($cinema as $ci) {
            $cinemaOption .= '<option value="' . $ci['id'] . '">' . $ci['name'] . '</option>';
        }
        echo json_encode(['status' => true, 'data' => $cinemaOption]);
    }

    public function actionGetDateByCinemaAndFilm()
    {
        $film_id = intval(Yii::$app->request->post('film_id'));
        $cinema_id = intval(Yii::$app->request->post('cinema_id'));
        $schedule = Schedule::getDateByCinemaAndFilm($film_id, $cinema_id);
        $date = [];
        foreach ($schedule as $key => $sc) {
            $d = date_format(date_create($sc['time']),'d-m-Y');
            if (!in_array($d, $date)) {
                $date[$key]['time'] = $d;
                $date[$key]['id'] = $sc['id'];
            }
        }
        $option = '';
        foreach ($date as $d) {
            $option .= '<option value="' . $d['id'] . '">' . $d['time'] . '</option>';
        }
        echo json_encode(['status' => true, 'data' => $option]);
    }

    public function actionGetTimeByDate()
    {
        $date = Yii::$app->request->post('date');
        $schedule = Schedule::getTimeByDate($date);
        $option = '';
        foreach ($schedule as $time) {
            $time['time'] = date_format(date_create($time['time']), 'H:i');
            $option .= '<option value="' . $time['id'] . '">' . $time['time'] . '</option>';
        }
        echo json_encode(['status' => true, 'data' => $option]);
    }
}
