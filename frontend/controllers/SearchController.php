<?php

namespace frontend\controllers;

use Yii;
use common\models\Film;

class SearchController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $keyword = strip_tags(Yii::$app->request->get('keyword'));
        if (strlen($keyword) > 0) {
            $films = Film::searchFilmByName($keyword);
        } else {
            $films = Film::getFilmScreened();
        }
        return $this->render('index', [
            'films' => $films
        ]);
    }

}
