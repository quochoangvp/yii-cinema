<?php

namespace frontend\controllers;

use Yii;
use common\models\News;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class TinTucDienAnhController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $query = News::find()->select('id, title, body, thumbnail')
            ->orderBy([
                'created_at' => SORT_DESC
            ]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'defaultPageSize' => 4]);
        $news = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', [
             'news' => $news,
             'pages' => $pages,
        ]);
    }

    public function actionChiTiet()
    {
        $id = intval(Yii::$app->request->get('id'));
        $news = News::find()->where(['id' => $id])->asArray()->one();
        if ($news) {
            $other_news = News::find()->where('id != ' . $id)->limit(4)->asArray()->all();
            return $this->render('chi-tiet', [
                'news' => $news,
                'other_news' => $other_news
            ]);
        } else {
            throw new NotFoundHttpException('Bài viết không tồn tại');
        }
    }
}
