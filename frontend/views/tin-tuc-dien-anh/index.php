<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\BaseStringHelper;
use yii\widgets\LinkPager;

$this->title = 'Tin tức điện ảnh';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        <!-- // -->
    </article>
    <section id="content">
        <div class="inner">
            <article class="general-block">
                <ul class="list-item comment-film">
                    <?php foreach ($news as $item): ?>
                    <li>
                        <figure>
                            <a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $item['id']) ?>">
                                <img src="<?= Url::to('@web/images/uploads/' . $item['thumbnail']) ?>" alt="">
                            </a>
                            <figcaption>
                                <h3>
                                    <a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $item['id']) ?>">
                                        <?= $item['title'] ?>
                                    </a>
                                </h3>
                                <p>
                                    <?= BaseStringHelper::byteSubstr(strip_tags($item['body']), 0, 420) ?>...
                                </p>
                                <a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $item['id']) ?>">Xem thêm ...</a>
                            </figcaption>
                        </figure>
                    </li>
                    <?php endforeach ?>
                </ul>
            </article>
        </div>
        <?= LinkPager::widget(['pagination' => $pages]); ?>
    </section>
</div>