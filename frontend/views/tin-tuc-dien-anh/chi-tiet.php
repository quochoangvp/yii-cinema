<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $news['title'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top tab-header-1">
        <h2 style="font-size: 24px;" class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        <!--  -->
    </article>
    <section id="content">
        <div class="crm-form" error="highlight,title">
            <div class="box" style="padding: 0px 12px 10px 12px;">
                <div class="box-content" style="color: #fff;line-height: 22px;margin-top: 0;">
                    <?= $news['body'] ?>
                </div>
            </div>
        </div>
        <header class="header-top"><h3 class="title smaller"><strong>Bài viết</strong> liên quan</h3></header>
        <ul class="row thumbnails related-thumbs">
            <?php foreach ($other_news as $news): ?>
            <li class="col-4">
                <a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $news['id']) ?>">
                    <img src="<?= Url::to('@web/images/uploads/' . $news['thumbnail']) ?>" alt="">
                </a>
                <div class="caption">
                    <h4><a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $news['id']) ?>">
                        <?= $news['title'] ?>
                    </a></h4>
                </div>
            </li>
            <?php endforeach ?>
        </ul>
    </section>
</div>