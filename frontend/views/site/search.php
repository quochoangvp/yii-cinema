<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Tìm kiếm phim';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <ul class="tab-header-1 larger">
        <li class="active"><a href="javascript:;" title="Tìm kiếm phim">Tìm kiếm phim</a></li>
    </ul>
    <div class="inner">
        <article class="general-block">
            <ul class="row thumbnails">
                <?php foreach ($filmScreened as $film): ?>
                <li class="col-4">
                    <a href="<?= Url::to('@web/site/chi-tiet/?slug=' . $film->slug) ?>">
                        <?= Html::img('@web/images/uploads/' . $film->thumb) ?>
                    </a>
                    <div class="caption">
                        <h4><?= Html::a($film->name, Url::to('@web/site/chi-tiet/?slug=' . $film->slug)); ?></h4>
                    </div>
                </li>
                <?php endforeach ?>
            </ul>
        </article>
        <div class="paging"></div>
    </div>
</div>