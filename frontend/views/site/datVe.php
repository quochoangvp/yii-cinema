<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Đặt vé';
$this->params['breadcrumbs'][] = $this->title;
?>

<form name="frmSelectTickets" method="post" action="" id="frmSelectTickets">
    <div id="placeholder" class="bg">
        <div class="box page-width">
            <span class="box-title"><span>Bạn đã chọn</span></span>
            <div class="box-content">
                <div class="left-center">
                    <div class="movie-detail">
                        <div class="movie-detail-left">
                            <img id="imgDetail" src="<?= Url::to('@web/images/uploads/' . $schedule['thumb']) ?>">
                        </div>
                        <div id="detailHolder" class="movie-detail-right">
                            <h2 class="movie-title"><?= $schedule['film_name'] ?></h2>
                            <div class="btns">
                                <span class="type"><span></span><?= $schedule['film_type_name'] ?></span>
                                <div class="clearfix"></div>
                            </div>
                            <div class="lbl cinema"><?= $schedule['cinema_name'] ?></div>
                            <div class="lbl date"><?= $schedule['date'] ?></div>
                            <div class="lbl time"><span><?= $schedule['time'] ?></span></div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="box movie-showtime">
                        <span class="box-title"><span>Vui lòng chọn vé</span></span>
                        <div id="showtime" class="box-content">
                            <div id="hint">
                                <span id="lblWelcome" class="TicketPageWelcome">
                                    Xin chào <strong><?= Yii::$app->user->identity->fullname ?></strong>
                                </span>
                                <span id="lblAboveTicketDetail" class="TicketPageText"><br><br>
                                    Vui lòng chọn số lượng và loại vé mà bạn muốn mua. Bạn có thể mua tối đa 10 vé cho mỗi lần giao dịch.<br><br>
                                    Trên website chỉ liệt kê <strong>vé dành cho người lớn</strong>.
                                    Mọi thắc mắc xin vui lòng liên hệ: <a href="mailto:supports@yii-cinema.com"><font color="#3BB9FF">supports@yii-cinema.com</font></a> để được giải đáp!</span>
                            </div>
                            <table id="tblTickets" cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr id="Tr1" class="TicketTypeHeaderRow">
                                    <td>
                                        <span id="lblTicketType" class="TicketTypeHeader">Loại vé</span>
                                    </td>
                                    <td width="80px" align="center">
                                        <span id="lblTicketQty" class="TicketTypeHeader">Số lượng</span>
                                    </td>
                                    <td width="100px" align="center">
                                        <span id="lblTicketUnitCost" class="TicketTypeHeader">Giá</span>
                                    </td>
                                    <td width="100px" align="right">
                                        <span id="lblTicketSubtotal" class="TicketTypeHeader">Tổng</span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <span class="TicketType TicketCategory"></span>
                                    </td>
                                </tr>

                                <tr class="TicketTypeRowAlt" ticketclass="NormalTicketTypeRow" areacategory="">
                                    <td valign="top" class="TicketTypeCell">
                                        <span class="TicketType">NGƯỜI LỚN</span>
                                        <span class="TicketTypeSubText"><br></span>
                                    </td>
                                    <td valign="top" align="right" class="TicketQtyCell">
                                        <select name="Ticket[ticket_qty]" id="" class="TicketTypeDropDown" onchange="updatePriceTotal(this)">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                        </select>
                                    </td>
                                    <td valign="top" class="Price">
                                        <span class="TicketTypePrice"><?= number_format($schedule['price']) ?></span> VND
                                    </td>
                                    <td valign="top" class="Price">
                                        <span class="TicketTypeSubTotal"><?= number_format($schedule['price']) ?></span> VND
                                    </td>
                                </tr>

                                <tr id="tblTotal" class="TicketTypeFooterRow">
                                    <td class="TicketTypeFooter" align="right" valign="top" colspan="3">
                                        <p style="margin-bottom: 5px;">Tổng cộng</p>
                                        <div>
                                            <span id="lblTax" class="Tax">Đã bao gồm VAT</span>
                                            <span id="lblBkFee" class="BkFee">* Miễn phí đặt vé</span>
                                        </div>
                                    </td>
                                    <td id="objOrderTotal" class="TicketTypeTotal" align="right"><span><?= number_format($schedule['price']) ?></span> VND</td>
                                </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="Ticket[film_type]" value="<?= $schedule['film_type_id'] ?>">
                            <input type="hidden" name="Ticket[schedule_id]" value="<?= $schedule['id'] ?>">
                            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>">
                            <div class="text-center error-message">
                                <?php
                                    if (isset($message) && strlen($message) > 0) {
                                        echo $message;
                                    }
                                ?>
                            </div>
                            <div id="buttons">
                                <input type="image" name="ibtnChangeSession" id="ibtnChangeSession" class="ImageChangeSession" src="<?= Url::to('@web/images/b4.png') ?>" border="0">
                                <input type="image" name="ibtnSelectSeats" id="ibtnSelectSeats" class="ImageSelectSeats" src="<?= Url::to('@web/images/payment.png') ?>" border="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
function updatePriceTotal(qtyEl) {
    var qty = $(qtyEl).find('option:selected').val();
    var price = parseInt($('.TicketTypePrice').text().replace(',', '')) * qty;

    $('.TicketTypeTotal>span').text(price.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace('.0', ''));
    $('.TicketTypeSubTotal').text(price.toFixed(1).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace('.0', ''));
}
$(document).ready(function() {
    $('#ibtnChangeSession').click(function(event) {
        event.preventDefault();
        window.history.back();
    });
    $('#ibtnSelectSeats').click(function(event) {
        event.preventDefault();
        cf = confirm('Xác nhận đặt vé?');
        if (cf) {
            $('#frmSelectTickets').submit();
        }
    });
});
</script>