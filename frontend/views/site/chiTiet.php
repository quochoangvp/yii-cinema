<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $film->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <article id="sidebar">

    </article>
    <section id="content">
        <div class="inner">
            <header class="header-top">
                <h2 class="title bold">Thông tin phim</h2>
            </header>
            <section class="general-block film-info detail">
                <div class="thumb">
                    <?= Html::img('@web/images/uploads/' . $film->thumb) ?>
                    <span class="play" data-toggle="modal"<?= ($film->trailer_link) ? ' data-target="#trailer-modal"':'' ?>>Play</span>
                </div>
                <article>
                    <header class="title-group">
                        <h3><?= $film->name ?></h3>
                    </header>
                    <div class="row-1">
                        <a href="#filmSchedule" title="Mua vé" class="btn-medium buy">Mua vé</a>
                    </div>
                    <p><strong>Khởi chiếu: </strong>Từ <?= $film->start ?> đến <?= $film->end ?></p>
                    <p><strong>Thể loại: </strong> Chung</p>
                    <p><strong>Diễn viên: </strong> Nguyễn Văn A</p>
                    <p><strong>Đạo diễn: </strong> Đạo diễn</p>
                </article>
                <div class="desc">
                    <p>
                        <?= $film->details ?>
                    </p>
                </div>
            </section>
            <div class="tab-header-1" id="filmSchedule">
                <h2>Lịch chiếu</h2>
            </div>
            <div class="silver-box full tab-content">
                <div class="tab-pane active" id="schedule-bundle-805">
                    <h2 class="silver-title">Các khung giờ</h2>
                    <div class="silver-content">
                        <?php if (isset($schedules[0])): ?>
                            <?php foreach ($schedules as $schedule): ?>
                            <div class="showtime">
                                <h3 class="showtime-title">
                                    <?= date_format(date_create($schedule['time']), 'd/m/Y') . ' (' . $schedule['cinema_name'] . ')' ?>
                                </h3>
                                <ul class="showtime-items">
                                    <li>
                                        <a href="<?= Url::to('@web/site/dat-ve/' . '?id=' . $schedule['id']) ?>">
                                            <?= date_format(date_create($schedule['time']), 'H:j') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <?php endforeach ?>
                        <?php else: ?>
                            Không có khung giờ nào
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal default fade" id="trailer-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" data-dismiss="modal" aria-label="Close">X</span>
                <h2 class="modal-title"><?= $film->name ?></h2>
            </div>
            <div class="modal-body">
                <iframe width="100%" height="500" src="<?= $film->trailer_link ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>