<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;

$this->title = 'Phim đang chiếu';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
	<ul class="tab-header-1 larger">
		<li class="active"><a href="javascript:;" title="Phim đang chiếu">Phim đang chiếu</a></li>
	</ul>
	<div class="inner">
		<article class="general-block">
			<ul class="row thumbnails">
				<?php foreach ($filmScreened as $film): ?>
				<li class="col-4">
					<a href="<?= Url::to('@web/site/chi-tiet/?slug=' . $film->slug) ?>">
						<?= Html::img('@web/images/uploads/' . $film->thumb) ?>
					</a>
					<div class="caption">
						<h4><?= Html::a($film->name, Url::to('@web/site/chi-tiet/?slug=' . $film->slug)); ?></h4>
					</div>
				</li>
				<?php endforeach ?>
			</ul>
		</article>
		<?= LinkPager::widget(['pagination' => $pages]); ?>
	</div>
</div>