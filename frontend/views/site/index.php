<?php

/* @var $this yii\web\View */
use \yii\helpers\Url;
use \yii\helpers\Html;

$this->title = 'ComTube Cinema - Đặt Vé Xem Phim Trực Tuyến Tại Nhà';
?>

<section class="banner">
    <div class="container">
        <div class="carousel slick-initialized slick-slider" id="banner">
            <div aria-live="polite" class="slick-list draggable" tabindex="0">
                <div class="slick-track">
                    <div class="slide-item slick-slide slick-active" data-slick-index="0" aria-hidden="false" style="width: 960px;">
                        <?= Html::img(Url::to('@web/images/uploads/960_61.jpg')) ?>
                        <div class="caption">
                            <div class="desc">
                                <h2>Huynh đệ tương tàn, Mỹ Nhân đại chiến</h2>
                                <p>Phòng vé tháng 4 sẽ nóng hơn bao giờ hết với những cuộc đại chiến giữa các siêu anh hùng và dàn mỹ nhân nóng bỏng. Xem ngay!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="film-block">
    <div class="container">
        <section class="ticket-booking hide-xs">
            <h2>Mua vé online</h2>
            <article class="cont">
                <form method="GET" action="" accept-charset="UTF-8" class="booking-form" name="booking-form">
                    <ul>
                        <li>
                            <div class="select-default select-film">
                                <span id="ticketFilm">Chọn phim</span>
                                <select class="film-booking" name="ticketFilm" onchange="selectFilm()">
                                    <option value="">Chọn phim</option>
                                    <?php foreach ($filmScreened as $film): ?>
                                        <option value="<?= $film['id'] ?>"><?= $film['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="select-default select-cinema">
                                <span id="ticketCinema">Chọn rạp</span>
                                <select class="cinema-booking" name="ticketCinema" onchange="selectCinema()">
                                    <option value="">Chọn rạp</option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="select-default select-date disabled">
                                <span id="ticketDate">Chọn ngày</span>
                                <select class="date-booking" name="ticketDate" onchange="selectDate()">
                                    <option value="">Chọn ngày</option>
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="select-default select-time disabled">
                                <span id="ticketSchedule">Chọn suất</span>
                                <select class="time-booking" name="ticketSchedule" onchange="selectSchedule()">
                                    <option value="">Chọn suất</option>
                                </select>
                            </div>
                        </li>
                    </ul>
                    <p class="text-center">
                    <button type="button" class="btn-buy" title="Mua ngay" id="btn-booking" onclick="datVe()">Mua ngay</button>
                    </p>
                </form>
            </article>
        </section>
        <section class="all-films">
            <ul class="tab-header">
                <li class="active">
                    <a title="Phim đang chiếu" aria-expanded="true" data-toggle="tab" href="javascript:;">Phim đang chiếu</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="slide-film-wrap slick-initialized slick-slider">
                    <div aria-live="polite" class="slick-list draggable" tabindex="0">
                        <div class="slick-track" style="opacity: 1; width: 1280px; transform: translate3d(0px, 0px, 0px);">
                            <div class="slider-film slick-slide slick-active" style="width: 640px;">
                                <?php foreach ($filmScreened as $film): ?>
                                <div class="film-intro">
                                    <div class="thumb">
                                        <a href="<?= Url::to('@web/site/chi-tiet/?slug=' . $film->slug) ?>">
                                            <?= Html::img(Url::to('@web/images/uploads/' . $film->thumb)) ?>
                                        </a>
                                        <?= Html::a(
                                            'Mua vé',
                                            Url::to('@web/site/chi-tiet/?slug=' . $film->slug . '#filmSchedule'),
                                            ['class' => 'buy']); ?>
                                    </div>
                                    <h4>
                                        <?= Html::a($film->name, Url::to('@web/site/chi-tiet/?slug=' . $film->slug)); ?>
                                    </h4>
                                </div>
                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text-center">
                    <a class="view-more" title="Xem thêm" href="<?= Url::to('@web/site/phim-dang-chieu') ?>">Xem thêm</a>
                </p>
            </div>
        </section>
    </div>
</section>
<div class="container">
    <article class="general-block">
        <header class="header-top">
            <h2 class="title bold">Tin tức điện ảnh mới</h2>
            <a href="<?= Url::to('@web/tin-tuc-dien-anh') ?>" title="Xem thêm" class="view-more-2">Xem thêm</a>
        </header>
        <ul class="row thumbnails">
        <?php foreach ($news as $item): ?>
            <li class="col-4">
                <a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $item['id']) ?>">
                    <img src="<?= Url::to('@web/images/uploads/' . $item['thumbnail']) ?>" alt="">
                </a>
                <div class="caption">
                    <h4><a href="<?= Url::to('@web/tin-tuc-dien-anh/chi-tiet/?id=' . $item['id']) ?>"><?= $item['title'] ?></a></h4>
                </div>
            </li>
        <?php endforeach ?>
        </ul>
    </article>
</div>