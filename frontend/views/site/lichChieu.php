<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Lịch chiếu phim';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
	<article id="sidebar">

	</article>
	<section id="content">
		<div class="inner">
			<header class="header-top">
				<h2 class="title bold">Lịch chiếu</h2>
			</header>

			<div class="general-block">
				<div class="arrow-left">
					<div class="silver-box">
						<h2 class="silver-title">Chọn phim</h2>
						<div id="movie" class="silver-content">
							<?php foreach ($films as $film): ?>
							<div class="item <?= ($currentFilm->id === $film->id) ? 'checked' : ''?>">
								<a href="<?= Url::to('@web/site/lich-chieu/?film_id=' . $film->id . '&cinema_id=' . $currentCinema->id) ?>">
									<?= $film->name ?>
								</a>
							</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
				<div class="block">
					<div class="arrow-bottom">
						<div class="silver-box">
							<h2 class="silver-title">Chọn rạp</h2>
							<div id="cinema" class="silver-content">
								<?php foreach ($cinemas as $cinema): ?>
									<div class="item <?= ($currentCinema->id === $cinema->id) ? 'checked' : ''?>">
										<a href="<?= Url::to('@web/site/lich-chieu/?film_id=' . $currentFilm->id . '&cinema_id=' . $cinema->id) ?>">
											<?= $cinema->name ?>
										</a>
									</div>
								<?php endforeach ?>
							</div>
						</div>
					</div>
					<div class="silver-box movie-time">
						<h2 class="silver-title">Chọn suất</h2>
						<div id="movie-time" class="silver-content">
						<?php if (isset($schedules[0])): ?>
							<?php foreach ($schedules as $schedule): ?>
							<div class="showtime">
								<h3 class="showtime-title"><?= date_format(date_create($schedule->time), 'd/m/Y') ?></h3>
								<ul class="showtime-items">
									<li>
										<a href="<?= Url::to('@web/site/dat-ve/?id=' . $schedule->id) ?>">
											<?= date_format(date_create($schedule->time), 'H:j') ?>
										</a>
									</li>
								</ul>
							</div>
							<?php endforeach ?>
						<?php else: ?>
							Không có lịch chiếu nào
						<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>