<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Đăng ký';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        
    </article>
    <section id="content">
        <div class="crm-form" error="highlight,title">
            <div class="box box-login">
                <span class="box-title">
                    Nhập thông tin đầy đủ để đăng ký
                </span>

                <div class="box-content">

                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'required' => true, 'class' => 'j-textbox'])->label('Tên đăng nhập') ?>

                    <?= $form->field($model, 'email')->textInput(['type' => 'email', 'required' => true, 'class' => 'j-textbox']) ?>

                    <?= $form->field($model, 'fullname')->textInput(['class' => 'j-textbox', 'required' => true])->label('Họ tên') ?>

                    <?= $form->field($model, 'mobile')->textInput(['class' => 'j-textbox', 'required' => true])->label('Số điện thoại') ?>

                    <?= $form->field($model, 'password')->passwordInput(['class' => 'j-textbox', 'required' => true])->label('Mật khẩu') ?>

                    <?= Html::submitButton('Đăng ký', ['class' => 'btn3', 'name' => 'login-signup']) ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>