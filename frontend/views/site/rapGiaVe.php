<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Rạp - Giá vé';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top">
        <h2 class="title">
            <strong>Thông tin</strong> Rạp
        </h2>
    </header>
    <article class="general-block cinema">
        <ul class="cinema-items">
            <?php foreach ($cinemas as $cinema): ?>
                <li class="<?= ($cinema->id == $cinemaActive->id) ? 'active' : '' ?>">
                    <a href="<?= Url::to('@web/site/rap-gia-ve/' . '?id=' . $cinema->id) ?>"><?= $cinema->name ?></a>
                </li>
            <?php endforeach ?>
        </ul>
        <div class="tab-content cinema-info">
            <div role="tabpanel" class="tab-pane fade active" id="cinema-5">
                <div class="cinema-left">
                    <h2 class="cinema-title"><?= $cinemaActive->name ?></h2>
                    <div class="cinema-desc">
                        <p><?= $cinemaActive->address ?></p>
                    </div>
                    <div class="cinema-desc">
                    </div>
                </div>
                <div class="cinema-right">
                    <div class="cinema-slider slick-initialized slick-slider">
                        <div aria-live="polite" class="slick-list draggable" tabindex="0">
                            <div class="slick-track" style="opacity: 1; width: 6610px; transform: translate3d(-661px, 0px, 0px);">
                                <div class="wrap slick-slide slick-cloned" style="width: 661px;">
                                    <?= Html::img('@web/images/uploads/' . $cinemaActive->image) ?>
                                </div>
                                <div class="wrap slick-slide slick-active" style="width: 661px;">
                                    <?= Html::img('@web/images/uploads/' . $cinemaActive->image) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article id="cinema-ticket-1" class="general-block ticket active">
        <div class="cinema-left">
            <h2 class="cinema-title">BẢNG GIÁ VÉ</h2>
            <ul class="cinema-items cinema-ticket">
                <li class="active"><a href="#ticket-11" data-toggle="tab" aria-expanded="true">2D/DIGITAL</a></li>
                <li><a href="#ticket-12" data-toggle="tab">3D</a></li>
            </ul>
        </div>
        <div class="cinema-right">
            <div class="tab-content cinema-ticket-cont">
                <div role="tabpanel" class="tab-pane fade active" id="ticket-11">
                    <img src="https://www.galaxycine.vn/media/cinema/price/TB_2D.jpg" alt="">
                </div>
                <div role="tabpanel" class="tab-pane fade" id="ticket-12">
                    <img src="https://www.galaxycine.vn/media/cinema/price/TB_3D.jpg" alt="">
                </div>
            </div>
        </div>
    </article>
</div>