<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Đặt vé thành công';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        
    </article>
    <section id="content">
        <div class="crm-form" error="highlight,title">
            <div class="box" style="padding: 0px 12px 10px 12px;">
                <div class="box-content" style="color: #fff;line-height: 22px;margin-top: 0;">
                Xin chúc mừng! Quý khách đã đặt vé thành công, chúng tôi sẽ sớm liên hệ lại với quý khách để xác nhận! <a href="<?= Url::to('@web') ?>">Trở về trang chủ</a>
                </div>
            </div>
        </div>
    </section>
</div>