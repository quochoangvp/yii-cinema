<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Liên hệ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        <!--  -->
    </article>
    <section id="content">
        <?php if (isset($message) && strlen($message) > 0): ?>
            <div class="contact-msg <?= ($status) ? 'success' : 'error' ?>"><?= $message ?></div>
        <?php endif ?>
        <div class="crm-form" error="highlight,title">
            <div class="box box-login">
                <div class="box-content">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                        <?= $form->field($model, 'name')->textInput(['required' => true,'autofocus' => true, 'class' => 'j-textbox'])->label('Họ tên') ?>

                        <?= $form->field($model, 'email')->textInput(['required' => true,'class' => 'j-textbox']) ?>

                        <?= $form->field($model, 'subject')->textInput(['required' => true,'class' => 'j-textbox'])->label('Tiêu đề') ?>

                        <?= $form->field($model, 'body')->textArea(['required' => true,'rows' => 6, 'class' => 'j-textarea'])->label('Nội dung') ?>

                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ])->label('Mã bảo mật') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Submit', ['class' => 'btn3', 'name' => 'contact-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>
