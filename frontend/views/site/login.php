<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Đăng nhập';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        <!--  -->
    </article>
    <section id="content">
        <div class="crm-form" error="highlight,title">
            <div class="box box-login">
                <span class="box-title">
                    Nhập thông tin chính xác để đăng nhập
                </span>

                <div class="box-content">

                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'j-textbox'])->label('Tên đăng nhập') ?>

                    <?= $form->field($model, 'password')->passwordInput(['class' => 'j-textbox'])->label('Mật khẩu') ?>

                    <?= Html::submitButton('Đăng nhập', ['class' => 'btn3', 'name' => 'login-button']) ?>

                    <a class="btn3" href="<?= Url::to('@web/site/signup') ?>">Đăng ký</a>

                    <div class="action-btns">
                        <?= $form->field($model, 'rememberMe')->checkbox()->label('Lưu đăng nhập') ?>
                        <?= Html::a('Quên mật khẩu?', ['ssite/request-password-reset'], ['id' => 'btnForgot']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>