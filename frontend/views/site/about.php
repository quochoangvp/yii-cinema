<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Giới thiệu';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <header class="header-top tab-header-1">
        <h2 class="title bold"><?= Html::encode($this->title) ?></h2>
    </header>
    <article id="sidebar">
        
    </article>
    <section id="content">
        <div class="crm-form" error="highlight,title">
            <div class="box" style="padding: 0px 12px 10px 12px;">
                <div class="box-content" style="color: #fff;line-height: 22px;margin-top: 0;">
                   	<?= $content ?>
                </div>
            </div>
        </div>
    </section>
</div>