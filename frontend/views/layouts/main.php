<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?= Html::cssFile('@web/css/slick.css'); ?>
    <?= Html::cssFile('@web/css/slick-theme.css'); ?>
    <?= Html::cssFile('@web/css/lightbox.css'); ?>
    <?= Html::cssFile('@web/css/jquery-ui.min.css'); ?>
    <?= Html::cssFile('@web/css/style.css'); ?>
    <?= Html::cssFile('@web/css/page.css'); ?>
    <?= Html::cssFile('@web/css/custom.css'); ?>
    <?= Html::cssFile('@web/css/medium.css', ['media' => 'max-width: 800px']); ?>
    <?= Html::cssFile('@web/css/print.css', ['media' => 'print']); ?>
    <?= Html::jsFile('@web/js/modernizr.js'); ?>
    <?= Html::jsFile('@web/js/jquery-1.12.3.min.js'); ?>
    <?= Html::jsFile('@web/js/bootstrap.min.js'); ?>
    <?= Html::jsFile('@web/js/common.js'); ?>
</head>
<body>
    <div class="page">
        <header id="header">
            <div class="container">
                <a class="logo" href="<?= Url::home() ?>">
                    <?= Html::img(Url::to('@web/images/logo.png')) ?>
                </a>
                <div class="inner">
                    <form method="GET" action="<?= Url::to('@web/search') ?>" accept-charset="UTF-8" class="search-form" name="search-form">
                        <input type="text" placeholder="Tìm kiếm" name="keyword" value="<?= Yii::$app->request->get('keyword') ?>" required="" />
                        <input type="submit" value="" />
                    </form>
                    <nav>
                        <ul>
                            <li><?= Html::a('Lịch chiếu', Url::to('@web/site/lich-chieu')) ?></li>
                            <li><?= Html::a('Phim', Url::to('@web/site/phim-dang-chieu')) ?></li>
                            <li><?= Html::a('Rạp/Giá vé', Url::to('@web/site/rap-gia-ve')) ?></li>
                            <li><?= Html::a('Tin tức điện ảnh', Url::to('@web/tin-tuc-dien-anh')) ?></li>
                            <li><?= Html::a('Giới thiệu', Url::to('@web/site/gioi-thieu')) ?></li>
                            <li><?= Html::a('Liên hệ', Url::to('@web/site/lien-he')) ?></li>
                            <li>
                            <?php
                            if (Yii::$app->user->isGuest) {
                                echo Html::a('Đăng nhập', Url::to('@web/site/login'));
                            } else {
                                echo Html::a('Đăng xuất (' . Yii::$app->user->identity->username . ')', Url::to('@web/site/logout'));
                            }
                            ?>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <main id="main">
            <?= $content ?>
        </main>

        <footer id="footer">
            <div class="container">
                <nav>
                    <ul>
                        <li><?= Html::a('Lịch chiếu', Url::to('@web/site/lich-chieu')) ?></li>
                        <li><?= Html::a('Phim', Url::to('@web/site/phim-dang-chieu')) ?></li>
                        <li><?= Html::a('Rạp/Giá vé', Url::to('@web/site/rap-gia-ve')) ?></li>
                    </ul>
                </nav>
                <div>
                    Hotline: 0987654321 - Email: admin@yii-cinema.com
                </div>
            </div>
        </footer>
    </div>
</body>
</html>