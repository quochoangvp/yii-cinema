<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Tìm kiếm phim: ' . Yii::$app->request->get('keyword');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <ul class="tab-header-1 larger">
        <li class="active"><a href="javascript:;"><?= $this->title ?></a></li>
    </ul>
    <div class="inner">
        <?php if (count($films) > 0): ?>
        <article class="general-block">
            <ul class="row thumbnails">
                <?php foreach ($films as $film): ?>
                <li class="col-4">
                    <a href="<?= Url::to('@web/site/chi-tiet/?slug=' . $film->slug) ?>">
                        <?= Html::img('@web/images/uploads/' . $film->thumb) ?>
                    </a>
                    <div class="caption">
                        <?php
                            $film->name = str_replace(mb_strtolower(Yii::$app->request->get('keyword'), 'UTF-8'), '<span class="highlight">' . ucwords(Yii::$app->request->get('keyword')) . '</span>', mb_strtolower($film->name, 'UTF-8'));
                            $film->name = ucwords($film->name);
                        ?>
                        <h4><?= Html::a($film->name, Url::to('@web/site/chi-tiet/?slug=' . $film->slug)); ?></h4>
                    </div>
                </li>
                <?php endforeach ?>
            </ul>
        </article>
        <div class="paging"></div>
        <?php else: ?>
            <p style="padding-bottom: 20px;">Không tìm thấy phim nào</p>
        <?php endif ?>
    </div>
</div>