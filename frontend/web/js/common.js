$(document).ready(function() {
	// find menu-item associated with this page and make current:
	$('#header a').each(function(index, value) {
	  if ($(this).prop('href') === window.location.href) {
	    $(this).parent().addClass('active');
	  }
	});
});

function selectFilm() {
	var filmId = $('select[name="ticketFilm"] option:selected').val();
	var filmName = $('select[name="ticketFilm"] option:selected').text();
	$('#ticketFilm').text(filmName);
	if (filmId > 0) {
		$.post('/yii-cinema/site/get-cinema-by-film', {film_id: filmId, _csrf: $('meta[name="csrf-token"]').attr('content')}, function(rs) {
			$('select[name="ticketCinema"]').html('<option value="">Chọn rạp</option>' + rs.data);
		}, 'json');
	}
}

function selectCinema() {
	var filmId = $('select[name="ticketFilm"] option:selected').val();
	var cinemaId = $('select[name="ticketCinema"] option:selected').val();
	var cinemaName = $('select[name="ticketCinema"] option:selected').text();
	$('#ticketCinema').text(cinemaName);
	if (filmId > 0 && cinemaId > 0) {
		$.post('/yii-cinema/site/get-date-by-cinema-and-film', {film_id: filmId, cinema_id: cinemaId, _csrf: $('meta[name="csrf-token"]').attr('content')}, function(rs) {
			$('select[name="ticketDate"]').html('<option value="">Chọn ngày</option>' + rs.data);
		}, 'json');
	}
}

function selectDate() {
	var scheduleId = $('select[name="ticketDate"] option:selected').val();
	var scheduleName = $('select[name="ticketDate"] option:selected').text();
	$('#ticketDate').text(scheduleName);
	if (scheduleId > 0) {
		$.post('/yii-cinema/site/get-time-by-date', {date: scheduleName, _csrf: $('meta[name="csrf-token"]').attr('content')}, function(rs) {
			$('select[name="ticketSchedule"]').html('<option value="">Chọn suất</option>' + rs.data);
		}, 'json');
	}
}

function selectSchedule() {
	var scheduleId = $('select[name="ticketSchedule"] option:selected').val();
	var scheduleName = $('select[name="ticketSchedule"] option:selected').text();
	$('#ticketSchedule').text(scheduleName);
	if (scheduleId > 0) {
		$('#btn-booking').attr('data-schedule-id', scheduleId);
	}
}

function datVe() {
	var id = $('#btn-booking').data('schedule-id');
	if (id > 0) {
		window.location.href = '/yii-cinema/site/dat-ve/?id=' + id;
	} else {
		alert('Hãy chọn đủ các thông tin cần thiết');
	}
}